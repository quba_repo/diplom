//
//  result.h
//  reac
//
//  Created by Anton Kudryashov on 11.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__result__
#define __reac__result__

#include <stdio.h>
#include "mesh.h"
#include "problem.h"
#include <fstream>

class result {
    class problem* problem;
    std::fstream FILE;
public:
    result(class problem* problem, const std::string &filename, std::ios_base::openmode mode);
    void compareSolution(int N);
    void compareSolutionInFinish(int N);
    void writeDownFunction(argument* arg);
    void writeValues(double value1, double value2);
    ~result();
};

#endif /* defined(__reac__result__) */
