//
//  mesh.h
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__mesh__
#define __reac__mesh__

#include <stdio.h>
#include <vector>

class mesh {
    int N1;
    int N2;
    int N3;
    double h1;
    double h2;
    double h3;
    double A1;
    double B1;
    double A2;
    double B2;
    double A3;
    double B3;
    int j;
    int**** indexes;
public:
    mesh();
    int* indx(int i, int s, int k);
    int* indxByDirection(int ibyDirection, int s, int k, int direction);
    int* prevIndx(int indx[3], int direction);
    int* nextIndx(int indx[3], int direction);
    int* firstIndx(int s, int k, int direction);
    int* lastIndx(int s, int k, int direction);
    double getA(int direction);
    int getN(int i);
    double getH(int direction);
    bool* getPbounds(int direction);
    bool* getVbounds(int direction);
    int getJ();
    bool directionIsCorrect(int direction);
    bool indxIsCorrect(int i, int s, int k);
    int* otherDirections(int direction);
    ~mesh();
};

#endif /* defined(__reac__mesh__) */
