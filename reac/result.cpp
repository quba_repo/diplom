//
//  result.cpp
//  reac
//
//  Created by Anton Kudryashov on 11.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "result.h"
#include "arrargument.h"
#include "funcargument.h"
#include "distargument.h"
#include "constants.h"

double f_p_t(double x1, double x2, double x3);
double f_v1_t(double x1, double x2, double x3);
double f_v2_t(double x1, double x2, double x3);
double f_v3_t(double x1, double x2, double x3);

result::result(class problem* currentProblem, const std::string &filename, std::ios_base::openmode mode)
{
    problem = currentProblem;
    FILE.open(filename, mode);
    FILE.precision(6);
}

result::~result()
{
    problem = NULL;
    FILE.close();
}

void result::compareSolution(int N)
{
    if (N==1)
        FILE << "N\tP\t\t\tV1\t\t\tV2\t\t\tV3\n";
    arrargument V1(problem->v1), V2(problem->v2), V3(problem->v3), P(problem->p);
    funcargument fv1(&f_v1_t, &problem->mesh), fv2(&f_v2_t, &problem->mesh), fv3(&f_v3_t, &problem->mesh), fp(&f_p_t, &problem->mesh);
    distargument distV1(&V1, &fv1), distV2(&V2, &fv2), distV3(&V3, &fv3), distP(&P, &fp);
    FILE << N << "\t" << problem->max(&distP) << "\t" << problem->max(&distV1) << "\t" << problem->max(&distV2) << "\t" << problem->max(&distV3) << "\n";
}

void result::compareSolutionInFinish(int N)
{
    arrargument V1(problem->v1), V2(problem->v2), V3(problem->v3), P(problem->p);
    funcargument fv1(&f_v1_t, &problem->mesh), fv2(&f_v2_t, &problem->mesh), fv3(&f_v3_t, &problem->mesh), fp(&f_p_t, &problem->mesh);
    distargument distV1(&V1, &fv1), distV2(&V2, &fv2), distV3(&V3, &fv3), distP(&P, &fp);
    char buff[150];
    sprintf(buff, "alpha=%f h1=%f h2=%f h3=%f tau=%f K=%d tau2=%f; eps1=%f; eps2=%f; iter2=%d; transfer=%d;\n\n",
            problem->alpha,
            problem->mesh.getH(1),
            problem->mesh.getH(2),
            problem->mesh.getH(3),
            problem->tau,
            N,
            problem->tau2,
            _epsilon1,
            _epsilon2,
            _iterations2,
            _withTransfer);
    std::string str = buff;
    FILE << str << problem->max(&distP) << "\t" << problem->max(&distV1) << "\t" << problem->max(&distV2) << "\t" << problem->max(&distV3) << std::endl << std::endl;
}

void result::writeDownFunction(argument *arg)
{
    for(int s=0;s<=problem->mesh.getN(2);s++){
        FILE << "ListLinePlot[{\n";
        for(int i=0;i<=problem->mesh.getN(1);i++){
            int* indx = problem->mesh.indx(i, s, 0);
            std::string str = i==problem->mesh.getN(1) ? "}\n" : "}, \n";
            FILE << "{" << i*problem->mesh.getH(1) << ", "<< arg[0][indx] << str;
        }
        FILE << "}]\n";
    }
}

void result::writeValues(double value1, double value2)
{
    FILE << value1 << "\t" << value2 << "\n";
}