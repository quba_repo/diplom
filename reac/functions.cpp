//
//  functions.cpp
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include "constants.h"

double f_mu(double x1, double x2, double x3)
{
    return 1.0/40.0;
}

double f_p(double x1, double x2, double x3)
{
    double r1 = a1;
    double r2 = b1;
    double w1 = _w1;
    double p0 = _p0;
    if (x1 > r1 && x1 < r2)
        return 0;
    return p0 + w1*w1*pow(r1,4)*( x1*x1/2.0 - 2.0*r2*r2*log(x1) - pow(r2,4)/(2.0*x1*x1) )/pow(pow(r2,2) - pow(r1,2),2);
}

double f_v1(double x1, double x2, double x3)
{
    return 0;
}

double f_v2(double x1, double x2, double x3)
{
    return 0;
}

double f_v3(double x1, double x2, double x3)
{
    double r1 = a1;
    double r2 = b1;
    double w1 = _w1;
    if (x1 > r1 && x1 < r2)
        return w1*r1*(r2-x1)/(r2-r1);
    return w1*r1*r1*(r2*r2-x1*x1)/(x1*(r2*r2-r1*r1));
}

double f_p_t(double x1, double x2, double x3)
{
    double r1 = a1;
    double r2 = b1;
    double w1 = _w1;
    double p0 = _p0;
    return p0 + w1*w1*pow(r1,4)*( x1*x1/2.0 - 2.0*r2*r2*log(x1) - pow(r2,4)/(2.0*x1*x1) )/pow(pow(r2,2) - pow(r1,2),2);
}
double f_v1_t(double x1, double x2, double x3)
{
    return 0;
}
double f_v2_t(double x1, double x2, double x3)
{
    return 0;
}
double f_v3_t(double x1, double x2, double x3)
{
    double r1 = a1;
    double r2 = b1;
    double w1 = _w1;
    return w1*r1*r1*(r2*r2-x1*x1)/(x1*(r2*r2-r1*r1));
}

double f_z11(double x1, double x2, double x3)
{
    return 1;
}

double f_z22(double x1, double x2, double x3)
{
    return 1;
}

double f_z33(double x1, double x2, double x3)
{
    return 1;
}

double f_H(double x1, double x2, double x3)
{
    int j = _j;
    return j==1 ? x1 : 1;
}