//
//  constants.h
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef reac_constants_h
#define reac_constants_h

#define pi 3.14159265

#define a1 1
#define b1 2
#define a2 0
#define b2 1
#define a3 0
#define b3 2*pi

#define _j 1
//работает, когда h<0
#define _N1 101
#define _N2 40
#define _N3 3

#define _alpha 0.6
#define _h1 0.01
#define _h2 0.5
#define _h3 pi

#define _tau 0.02
#define _epsilon1 0.0001
#define _iterations1 10000

#define _tau2 1
#define _epsilon2 0.001
#define _iterations2 100
#define _C 0

#define _w1 1
#define _w2 0

#define _p0 0

#define _p_left1 0
#define _p_right1 0
#define _p_left2 1
#define _p_right2 1
#define _p_left3 0
#define _p_right3 0

#define _v_left1 0
#define _v_right1 0
#define _v_left2 1
#define _v_right2 1
#define _v_left3 0
#define _v_right3 0

#define _withTransfer 0
#define _transferableDirection 2

#endif
