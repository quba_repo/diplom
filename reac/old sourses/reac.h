#include <iostream>
#include <fstream>
#include <math.h>

class reac{
	double h1;//setka
	double h2;
	double h3;
    double x1;
    double x2;
	int N1;
	int N2;
	int N3;
	double tau;
	double alpha;
	int j;//0-dekart;1-cilindr
	double*** v1;
	double*** v2;
	double*** v3;
	double*** p;
	double eps1;
	double eps2;
	double eps3;
	double epsp;
public:
	reac(double H1,double H2,double H3,double TAU,int J,double alp);
	reac(double H1,double H2,double H3,double TAU,int J,double alp,bool yes);
	double RS(int tt);
	double get(int num,int i,int s,int k);
	double Lph(double*** ksi,int i,int s,int k);
	double get_eps(int num);
	void stream_function(double tok);
    double derivative(int symm,
                      double secondValue,
                      double midValue ,
                      double firstValue,
                      int direction,
                      int i,
                      int s,
                      int k)
    {
        double h;
        double*** v;
        switch (direction) {
            default:
                h = h1;
                v = v1;
                break;
            case 2:
                h = h2;
                v = v2;
                break;
            case 3:
                h = h3;
                v = v3;
                break;
        }
        //std::cout << "ss" << ss << "\n";
        float a = 1;
        int sign = v[i][s][k] > 0 ? 1 : -1;
        
        double chisl = fabs(secondValue - midValue) + fabs(midValue - firstValue);
        double e = chisl < 0.000001 ? 0 : fabs(firstValue - 2*midValue + secondValue)/chisl;
        
        return (secondValue - firstValue)/(2*h) - a*symm*sign*e*(firstValue - 2*midValue + secondValue)/(2*h);
    };
    double smoothCoef(bool central, double*** ksi, int direction, int i, int s, int k)
    {
        int in1 = 0;
        int in2 = 0;
        int in3 = 0;
        double h;
        double*** v;
        switch (direction) {
            default:
                h = h1;
                v = v1;
                in1 = 1;
                break;
            case 2:
                h = h2;
                v = v2;
                in2 = 1;
                break;
            case 3:
                h = h3;
                v = v3;
                in3 = 1;
                break;
        }
        float a = 1;
        int sign = v[i][s][k] > 0 ? 1 : -1;
        int kDecrement = k == 0? N3-1 : k-in3;
        double chisl = fabs(ksi[i + in1][s + in2][k + in3] - ksi[i][s][k])
                       + fabs(ksi[i][s][k] - ksi[i - in1][s - in2][kDecrement]);
        double e = chisl < 0.000001
        ? 0 : fabs(ksi[i + in1][s + in2][k + in3] - 2*ksi[i][s][k] + ksi[i - in1][s - in2][kDecrement])/chisl;
        return central ? a*sign*e/h : -a*sign*e/(2*h);
    };
};