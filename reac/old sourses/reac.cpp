#include "reac.h"
#include <math.h>
#include <iostream>
#include <fstream>

int kDerTypeContitunity = 0;//1;
int kDerTypeVelocity = 0;//1;
int kDerTypePuasson = 0;
int kDerTypePuassonEND = 0;//-1;
int kDerTypeSteps = 0;
int kDerTypePressure = 0;//-1;

double r1 = 1;
double r2 = 2;
double w1 = 1;
double w2 = 0;
double CO = 0;//=wC/w1
double wC = CO*w1;
double G = 2;

double tau2 = 0.05;
double eps = 0.01;
double epsRange = -1;
int OMEGA = 5;//inner iter

double S = pow(w1*r1*r1/(r2*r2-r1*r1),2);
double CP = 0;

int sigma = 0;

double p_0 = 1;
#define pi 3.14159265

double mu(double x1,double x2,double x3){
	return 1.0/40;
}
double p_(double x1,double x2,double x3){
	//return p_0 + pow(w1,2.0)*pow(r1,4.0)*( (x1*x1)/2.0 - 2.0*r2*r2*log(x1) - pow(r2,4.0)/(2.0*x1*x1) )/pow(pow(r2,2.0) - pow(r1,2.0),2);
	return p_0  + CP*x2 + S*(x1*x1/2.0 - 2*r2*r2*log(x1) - pow(r2,4)/(2.0*x1*x1));//j=1
	//return 5;
	
	//return p_0 - 4*mu(x1,x2,x3)*x2;
	
}
double v_1(double x1,double x2,double x3){
	return 0;
}
double v_2(double x1,double x2,double x3){
	return 0;
	/*if(x2 > 0 && x2 < 0 && x1 > 0)
		return 0;
	else{
		return (1 - x1*x1);
	}*/
	//return (1 - x1*x1);
	//return CP*((r1*r1-r2*r2)*log(x1/r2)/log(r1/r2) + x1*x1 - r1*r1)/4.0;
	
//	if(x1 == r1 || x1 == r2){
//		return 0;
//	}
//	else{
	//	return 100;
	//}
}
double v_3(double x1,double x2,double x3){
	//return (-w1*r1*r1*x1*x1 + w1*r1*r1*r2*r2)/(x1*(r2*r2-r1*r1));
	return w1*r1*r1*(r2*r2-x1*x1)/(x1*(r2*r2-r1*r1)); //1zadacha
	//return 0;

	
	/*double delta = 0.33;
	if((x1 > r1 + delta) && (x1 < r2 - delta) && (x2 > delta) && (x2 < 1-delta) && (fabs(x3) < delta) )
		return 0;
	else
		return w1*r1*r1*(r2*r2-x1*x1)/(x1*(r2*r2-r1*r1));
*/
	if((x2==0 || x2 ==G) && x1>r1 && x1<r2)
		return wC*x1;
	if(x1==r2)
		return 0;
	else
		return w1*r1;
}
double dqx(double x1,double x2,double x3,int i,int j){
	return 1;
}
double H(double r,int j){
	if(j==1)
		return r;
	else
		return 1;
}

double v1t(double x1,double x2,double x3){
	return 0;
}
double v2t(double x1,double x2,double x3){
	//return CP*((r1*r1-r2*r2)*log(x1/r2)/log(r1/r2) + x1*x1 - r1*r1)/4.0;
	//return (1 - x1*x1);
	return 0;
}
double v3t(double x1,double x2,double x3){
	//return 0;
	return w1*r1*r1*(r2*r2-x1*x1)/(x1*(r2*r2-r1*r1));
}
double P_t(double x1,double x2,double x3){
	//return p_0;// - 4*mu(x1,x2,x3)*x2;
	return p_0 + S*(x1*x1/2.0 - 2*r2*r2*log(x1) - pow(r2,4)/(2.0*x1*x1));
}

reac::reac(double H1, double H2, double H3, double TAU, int J,double alp){
	h1=H1;
	h2=H2;
	h3=H3;
    x1=r1;
    x2=r2;
	tau=TAU;
	eps1 = 5000;
	eps2 = 5000;
	eps3 = 5000;
	epsp = 5000;
	N1 = (int)((r2-r1)/h1);
	N2 = (int)(G/h2);
	N3 = (int)(2.0*pi/h3);
	j=J;
	alpha = alp;
	v1 = new double**[N1+1];
	v2 = new double**[N1+1];
	v3 = new double**[N1+1];
	p = new double**[N1+1];
	for(int i=0;i<=N1;i++){
		v1[i] = new double*[N2+1];
		v2[i] = new double*[N2+1];
		v3[i] = new double*[N2+1];
		p[i] = new double*[N2+1];
	}
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			v1[i][s] = new double[N3+1];
			v2[i][s] = new double[N3+1];
			v3[i][s] = new double[N3+1];
			p[i][s] = new double[N3+1];
		}
	}
	
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			for(int k=0;k<=N3;k++){
				v1[i][s][k]=v_1(r1+h1*i,h2*s,h3*k);
				v2[i][s][k]=v_2(r1+h1*i,h2*s,h3*k);
				v3[i][s][k]=v_3(r1+h1*i,h2*s,h3*k);
				p[i][s][k]=p_(r1+h1*i,h2*s,h3*k);
			}
		}
	}
	for(int s=0;s<=N2;s++){
		for(int k=0;k<=N3;k++){
			p[N1][s][k] = (4.0*p[N1-1][s][k] - p[N1-2][s][k])/3.0;
			p[1][s][k] = (4.0*p[1][s][k]-p[2][s][k])/3.0;
		}
	}
	for(int i=0;i<=N1;i++){
		for(int k=0;k<=N3;k++){
			p[i][N2][k] = (4.0*p[i][N2-1][k]-p[i][N2-2][k])/3.0;
			p[i][0][k] = (4.0*p[i][1][k]-p[i][2][k])/3.0;
		}
	}
}
reac::reac(double H1,double H2,double H3,double TAU,int J,double alp,bool yes){
	h1=H1;
	h2=H2;
	h3=H3;
    x1=r1;
    x2=r2;
	tau=TAU;
	eps1 = 5000;
	eps2 = 5000;
	eps3 = 5000;
	epsp = 5000;
	N1 = (int)((r2-r1)/h1);
	N2 = (int)(G/h2);
	N3 = (int)(2.0*pi/h3);
	j=J;
	alpha = alp;
	v1 = new double**[N1+1];
	v2 = new double**[N1+1];
	v3 = new double**[N1+1];
	p = new double**[N1+1];
	for(int i=0;i<=N1;i++){
		v1[i] = new double*[N2+1];
		v2[i] = new double*[N2+1];
		v3[i] = new double*[N2+1];
		p[i] = new double*[N2+1];
	}
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			v1[i][s] = new double[N3+1];
			v2[i][s] = new double[N3+1];
			v3[i][s] = new double[N3+1];
			p[i][s] = new double[N3+1];
		}
	}
	std::ifstream FILEv1("outs1.txt");
	std::ifstream FILEv2("outs2.txt");
	std::ifstream FILEv3("outs3.txt");
	std::ifstream FILEp("outp.txt");
	
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			for(int k=0;k<=N3;k++){
				FILEv1 >> v1[i][s][k];
				FILEv2 >> v2[i][s][k];
				FILEv3 >> v3[i][s][k];
				FILEp >> p[i][s][k];
			}
		}
	}
	FILEv1.close();
	FILEv2.close();
	FILEv3.close();
	FILEp.close();
}
double reac::RS(int tt){
	double param = 0;
	//int M = (int)(1.0/tau);
	double*** ksi_p = new double**[N1+1];
	double*** ksi_1 = new double**[N1+1];
	double*** ksi_2 = new double**[N1+1];
	double*** ksi_3 = new double**[N1+1];
	for(int i=0;i<=N1;i++){
		ksi_p[i] = new double*[N2+1];
		ksi_1[i] = new double*[N2+1];
		ksi_2[i] = new double*[N2+1];
		ksi_3[i] = new double*[N2+1];
	}
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			ksi_p[i][s] = new double[N3+1];
			ksi_1[i][s] = new double[N3+1];
			ksi_2[i][s] = new double[N3+1];
			ksi_3[i][s] = new double[N3+1];
		}
	}
    
	for(int i=1;i<N1;i++){
		for(int s=1;s<N2;s++){
			for(int k=1;k<N3;k++){
                ksi_p[i][s][k] = (-1.0)*((dqx(r1+h1*i,h2*s,h3*k,1,1)*derivative(kDerTypeContitunity,
                                                                                H(r1+h1*(i+1),j)*v1[i+1][s][k],
                                                                                H(r1+h1*i,j)*v1[i][s][k],
                                                                                H(r1+h1*(i-1),j)*v1[i-1][s][k],
                                                                                1, i, s, k)
                                         + dqx(r1+h1*i,h2*s,h3*k,3,3)*derivative(kDerTypeContitunity,
                                                                                 v3[i][s][k+1],
                                                                                 v3[i][s][k],
                                                                                 v3[i][s][k-1],
                                                                                 3, i, s, k)
                                          )/H(r1+h1*i,j)
                                         + dqx(r1+h1*i,h2*s,h3*k,2,2)*derivative(kDerTypeContitunity,
                                                                                 v2[i][s+1][k],
                                                                                 v2[i][s][k],
                                                                                 v2[i][s-1][k],
                                                                                 2, i, s, k)
                                         );
				//if(ksi_p[i][s][k]!=0){
				//	std::cout <<"ksi "<< ksi_p[i][s][k] << "\n";
				//}
				double Hmu0_5 = 0.5*(H(r1+(i+1)*h1,j)*mu(r1+(i+1)*h1,s*h2,k*h3)*dqx(r1+(i+1)*h1,s*h2,k*h3,1,1)
                                     + H(r1+i*h1,j)*mu(r1+i*h1,s*h2,k*h3)*dqx(r1+i*h1,s*h2,k*h3,1,1));
				double Hmu_0_5= 0.5*(H(r1+(i-1)*h1,j)*mu(r1+(i-1)*h1,s*h2,k*h3)*dqx(r1+(i-1)*h1,s*h2,k*h3,1,1)
                                     + H(r1+i*h1,j)*mu(r1+i*h1,s*h2,k*h3)*dqx(r1+i*h1,s*h2,k*h3,1,1));
				double HH05 = 0.5*(mu(r1+h1*i,h2*(s+1),h3*k)*dqx(r1+h1*i,h2*(s+1),h3*k,2,2)
                                   + mu(r1+h1*i,h2*s,h3*k)*dqx(r1+h1*i,h2*s,h3*k,2,2));
				double HH05_= 0.5*(mu(r1+h1*i,h2*(s-1),h3*k)*dqx(r1+h1*i,h2*(s-1),h3*k,2,2)
                                   + mu(r1+h1*i,h2*s,h3*k)*dqx(r1+h1*i,h2*s,h3*k,2,2));
				double H305 = 0.5*(mu(r1+h1*i,h2*s,h3*(k+1))*dqx(r1+h1*i,h2*s,h3*(k+1),3,3)
                                   + mu(r1+h1*i,h2*s,h3*k)*dqx(r1+h1*i,h2*s,h3*k,3,3));
				double H305_= 0.5*(mu(r1+h1*i,h2*s,h3*(k-1))*dqx(r1+h1*i,h2*s,h3*(k-1),3,3)
                                   + mu(r1+h1*i,h2*s,h3*k)*dqx(r1+h1*i,h2*s,h3*k,3,3));
				ksi_1[i][s][k] = (-1.0)*(dqx(r1+h1*i,h2*s,h3*k,1,1)*(derivative(kDerTypeVelocity,
                                                                                H(r1+h1*(i+1),j)*pow(v1[i+1][s][k],2),
                                                                                H(r1+h1*i,j)*pow(v1[i][s][k],2),
                                                                                H(r1+h1*(i-1),j)*pow(v1[i-1][s][k],2),
                                                                                1, i, s, k)
					- 2.0*(Hmu0_5*(v1[i+1][s][k]-v1[i][s][k]) - Hmu_0_5*(v1[i][s][k]-v1[i-1][s][k]))/(h1*h1)
                                                                     )/H(r1+h1*i,j)
                    + dqx(r1+i*h1,s*h2,k*h3,1,1)*derivative(kDerTypePressure,
                                                            p[i+1][s][k],
                                                            p[i][s][k],
                                                            p[i-1][s][k],
                                                            1, i, s, k)
                    - j*(
                         pow(v3[i][s][k],2) - 2.0*mu(r1+i*h1,s*h2,k*h3)*v1[i][s][k]/H(r1+i*h1,j)
                         )/H(r1+i*h1,j)
                                         + dqx(r1+h1*i,h2*s,h3*k,2,2)*(derivative(kDerTypeVelocity,
                                                                                  v1[i][s+1][k]*v2[i][s+1][k],
                                                                                  v1[i][s][k]*v2[i][s][k],
                                                                                  v1[i][s-1][k]*v2[i][s-1][k],
                                                                                  2, i, s, k)
					- (mu(r1+h1*i,h2*(s+1),h3*k)*dqx(r1+h1*i,h2*(s+1),h3*k,1,1)*(v2[i+1][s+1][k]-v2[i-1][s+1][k]) - mu(r1+h1*i,h2*(s-1),h3*k)*dqx(r1+h1*i,h2*(s-1),h3*k,1,1)*(v2[i+1][s-1][k]-v2[i-1][s-1][k]) )/(4.0*h1*h2)
					- (HH05*(v1[i][s+1][k]-v1[i][s][k]) - HH05_*(v1[i][s][k]-v1[i][s-1][k]))/(h2*h2)
                                                  )
                     + dqx(r1+h1*i,h2*s,h3*k,3,3)*(derivative(kDerTypeVelocity,
                                                              v1[i][s][k+1]*v3[i][s][k+1],
                                                              v1[i][s][k]*v3[i][s][k],
                                                              v1[i][s][k-1]*v3[i][s][k-1],
                                                              3, i, s, k)
                    - (mu(r1+h1*i,h2*s,h3*(k+1))*dqx(r1+h1*i,h2*s,h3*(k+1),1,1)*(v3[i+1][s][k+1]-v3[i-1][s][k+1]) - mu(r1+h1*i,h2*s,h3*(k-1))*dqx(r1+h1*i,h2*s,h3*(k-1),1,1)*(v3[i+1][s][k-1]-v3[i-1][s][k-1])
                       )/(4.0*h1*h3)
					- (H305*(v1[i][s][k+1]-v1[i][s][k]) - H305_*(v1[i][s][k]-v1[i][s][k-1]) )/(H(r1+h1*i,j)*h3*h3)
                    + j*(3.0*mu(r1+i*h1,s*h2,k*h3)*derivative(kDerTypeVelocity,
                                                              v3[i][s][k+1],
                                                              v3[i][s][k],
                                                              v3[i][s][k-1],
                                                              3, i, s, k)
                         + v3[i][s][k]*derivative(kDerTypeVelocity,
                                                  mu(r1+i*h1,s*h2,(k+1)*h3),
                                                  mu(r1+i*h1,s*h2,k*h3),
                                                  mu(r1+i*h1,s*h2,(k-1)*h3),
                                                  3, i, s, k) )/H(r1+h1*i,j)
                                                  )/H(r1+h1*i,j)
                                         );
				
					//std::cout <<"ksi "<< ksi_1[i][s][k] << "\n";
				
				double mudq22 = 0.5*(mu(r1+i*h1,(s+1)*h2,k*h3)*dqx(r1+i*h1,(s+1)*h2,k*h3,2,2) + mu(r1+i*h1,s*h2,k*h3)*dqx(r1+i*h1,s*h2,k*h3,2,2));
				double mudq22_ = 0.5*(mu(r1+i*h1,(s-1)*h2,k*h3)*dqx(r1+i*h1,(s-1)*h2,k*h3,2,2) + mu(r1+i*h1,s*h2,k*h3)*dqx(r1+i*h1,s*h2,k*h3,2,2));
				ksi_2[i][s][k]= (-1.0)*(dqx(r1+i*h1,s*h2,k*h3,1,1)*(derivative(kDerTypeVelocity,
                                                                               v1[i+1][s][k]*v2[i+1][s][k]*H(r1+h1*(i+1),j),
                                                                               v1[i][s][k]*v2[i][s][k]*H(r1+h1*i,j),
                                                                               v1[i-1][s][k]*v2[i-1][s][k]*H(r1+h1*(i-1),j),
                                                                               1, i, s, k)
					- (Hmu0_5*(v2[i+1][s][k]-v2[i][s][k]) - Hmu_0_5*(v2[i][s][k]-v2[i-1][s][k]))/(h1*h1)
					- dqx(r1+i*h1,s*h2,k*h3,2,2)*(H(r1+(i+1)*h1,j)*mu(r1+(i+1)*h1,s*h2,k*h3)*(v1[i+1][s+1][k] - v1[i+1][s-1][k]) - H(r1+(i-1)*h1,j)*mu(r1+(i-1)*h1,s*h2,k*h3)*(v1[i-1][s+1][k] - v1[i-1][s-1][k]) )/(4.0*h1*h2)
                                                                    )/H(r1+h1*i,j)
                    + dqx(r1+i*h1,s*h2,k*h3,2,2)*((derivative(kDerTypeVelocity,
                                                             pow(v2[i][s+1][k],2),
                                                             pow(v2[i][s][k],2),
                                                             pow(v2[i][s-1][k],2),
                                                             2, i, s, k)
                                                   + derivative(kDerTypePressure,
                                                                p[i][s+1][k],
                                                                p[i][s][k],
                                                                p[i][s-1][k],
                                                                2, i, s, k)
                                                   )
					- 2.0*(mudq22*(v2[i][s+1][k]-v2[i][s][k]) - mudq22_*(v2[i][s][k]-v2[i][s-1][k]) )/(h2*h2)
					) + dqx(r1+i*h1,s*h2,k*h3,3,3)*(derivative(kDerTypeVelocity,
                                                               v2[i][s][k+1]*v3[i][s][k+1],
                                                               v2[i][s][k]*v3[i][s][k],
                                                               v2[i][s][k-1]*v3[i][s][k-1],
                                                               3, i, s, k)
					- dqx(r1+i*h1,s*h2,k*h3,2,2)*(mu(r1+i*h1,s*h2,(k+1)*h3)*(v3[i][s+1][k+1]-v3[i][s-1][k+1]) - mu(r1+i*h1,s*h2,(k-1)*h3)*(v3[i][s+1][k-1]-v3[i][s-1][k-1]) )/(4.0*h2*h3)
					- ( H305*(v2[i][s][k+1]-v2[i][s][k]) - H305_*(v2[i][s][k]-v2[i][s][k-1]) )/(H(r1+h1*i,j)*h3*h3)
                                                    )/H(r1+i*h1,j));

				double HHmudq11 = 0.5*(pow(H(r1+(i+1)*h1,j),2)*mu(r1+(i+1)*h1,s*h2,k*h3)*dqx(r1+(i+1)*h1,s*h2,k*h3,1,1)
                                       + pow(H(r1+i*h1,j),2)*mu(r1+i*h1,s*h2,k*h3)*dqx(r1+i*h1,s*h2,k*h3,1,1) );
				double HHmudq11_ = 0.5*(pow(H(r1+(i-1)*h1,j),2)*mu(r1+(i-1)*h1,s*h2,k*h3)*dqx(r1+(i-1)*h1,s*h2,k*h3,1,1)
                                        + pow(H(r1+i*h1,j),2)*mu(r1+i*h1,s*h2,k*h3)*dqx(r1+i*h1,s*h2,k*h3,1,1) );
				ksi_3[i][s][k] = (-1.0)*(dqx(r1+i*h1,s*h2,k*h3,1,1)*(derivative(kDerTypeVelocity,
                                                                                pow(H(r1+(i+1)*h1,j),2)*v1[i+1][s][k]*v3[i+1][s][k],
                                                                                pow(H(r1+i*h1,j),2)*v1[i][s][k]*v3[i][s][k],
                                                                                pow(H(r1+(i-1)*h1,j),2)*v1[i-1][s][k]*v3[i-1][s][k],
                                                                                1, i, s, k)
					-(HHmudq11*(v3[i+1][s][k]-v3[i][s][k]) - HHmudq11_*(v3[i][s][k]-v3[i-1][s][k]) )/(h1*h1)
					- dqx(r1+i*h1,s*h2,k*h3,3,3)*(H(r1+(i+1)*h1,j)*mu(r1+(i+1)*h1,s*h2,k*h3)*(v1[i+1][s][k+1]-v1[i+1][s][k-1]) - H(r1+(i-1)*h1,j)*mu(r1+(i-1)*h1,s*h2,k*h3)*(v1[i-1][s][k+1]-v1[i-1][s][k-1]) )/(4.0*h1*h3)
                                                                     )/pow(H(r1+i*h1,j),2)
					+ dqx(r1+i*h1,s*h2,k*h3,2,2)*(derivative(kDerTypeVelocity,
                                                             v2[i][s+1][k]*v3[i][s+1][k],
                                                             v2[i][s][k]*v3[i][s][k],
                                                             v2[i][s-1][k]*v3[i][s-1][k],
                                                             2, i, s, k)
					- (mudq22*(v3[i][s+1][k]-v3[i][s][k]) - mudq22_*(v3[i][s][k]-v3[i][s-1][k]) )/(h2*h2)
					- dqx(r1+i*h1,s*h2,k*h3,3,3)*(mu(r1+i*h1,(s+1)*h2,k*h3)*(v2[i][s+1][k+1]-v2[i][s+1][k-1]) - mu(r1+i*h1,(s-1)*h2,k*h3)*(v2[i][s-1][k+1]-v2[i][s-1][k-1]) )/(H(r1+i*h1,j)*4.0*h2*h3)
					) + dqx(r1+i*h1,s*h2,k*h3,3,3)*((derivative(kDerTypeVelocity,
                                                                pow(v3[i][s][k+1],2),
                                                                pow(v3[i][s][k],2),
                                                                pow(v3[i][s][k-1],2),
                                                                3, i, s, k)
                                                      + derivative(kDerTypePressure,
                                                                   p[i][s][k+1],
                                                                   p[i][s][k],
                                                                   p[i][s][k-1],
                                                                   3, i, s, k)
                                                     )
					- 2.0*( H305*(v3[i][s][k+1]-v3[i][s][k]) - H305_*(v3[i][s][k]-v3[i][s][k-1]) )/(H(r1+h1*i,j)*h3*h3)
					)/H(r1+i*h1,j) + j*(
					 dqx(r1+i*h1,s*h2,k*h3,1,1)*derivative(kDerTypeVelocity,
                                                           mu(r1+(i+1)*h1,s*h2,k*h3)*H(r1+(i+1)*h1,j)*v3[i+1][s][k],
                                                           mu(r1+i*h1,s*h2,k*h3)*H(r1+i*h1,j)*v3[i][s][k],
                                                           mu(r1+(i-1)*h1,s*h2,k*h3)*H(r1+(i-1)*h1,j)*v3[i-1][s][k],
                                                           1, i, s, k)
					- 2.0*dqx(r1+i*h1,s*h2,k*h3,3,3)*derivative(kDerTypeVelocity,
                                                                mu(r1+i*h1,s*h2,(k+1)*h3)*v1[i][s][k+1],
                                                                mu(r1+i*h1,s*h2,k*h3)*v1[i][s][k],
                                                                mu(r1+i*h1,s*h2,(k-1)*h3)*v1[i][s][k-1],
                                                                3, i, s, k)
                                        )/pow(H(r1+i*h1,j),2));
			}
		}
	}
	if(j==1){
		for(int i=1;i<N1;i++){
			for(int s=1;s<N2;s++){
                ksi_p[i][s][0] = (-1.0)*((dqx(r1+h1*i,h2*s,0,1,1)*derivative(kDerTypeContitunity,
                                                                             H(r1+h1*(i+1),j)*v1[i+1][s][0],
                                                                             H(r1+h1*i,j)*v1[i][s][0],
                                                                             H(r1+h1*(i-1),j)*v1[i-1][s][0],
                                                                             1, i, s, 0)
                                          + dqx(r1+h1*i,h2*s,0,3,3)*derivative(kDerTypeContitunity,
                                                                               v3[i][s][1],
                                                                               v3[i][s][0],
                                                                               v3[i][s][N3-1],
                                                                               3, i, s, 0))/H(r1+h1*i,j)
                                         + dqx(r1+h1*i,h2*s,0,2,2)*derivative(kDerTypeContitunity,
                                                                              v2[i][s+1][0],
                                                                              v2[i][s][0],
                                                                              v2[i][s-1][0],
                                                                              2, i, s, 0)
                                         );
				//if(ksi_p[i][s][0]!=0){
				//	std::cout <<"ksi "<< ksi_p[i][s][0] << "\n";
				//}
				double Hmu0_5 = 0.5*(H(r1+(i+1)*h1,j)*mu(r1+(i+1)*h1,s*h2,0)*dqx(r1+(i+1)*h1,s*h2,0,1,1)
                                     + H(r1+i*h1,j)*mu(r1+i*h1,s*h2,0)*dqx(r1+i*h1,s*h2,0,1,1));
				double Hmu_0_5= 0.5*(H(r1+(i-1)*h1,j)*mu(r1+(i-1)*h1,s*h2,0)*dqx(r1+(i-1)*h1,s*h2,0,1,1)
                                     + H(r1+i*h1,j)*mu(r1+i*h1,s*h2,0)*dqx(r1+i*h1,s*h2,0,1,1));
				double HH05 = 0.5*(mu(r1+h1*i,h2*(s+1),0)*dqx(r1+h1*i,h2*(s+1),0,2,2)
                                   + mu(r1+h1*i,h2*s,0)*dqx(r1+h1*i,h2*s,0,2,2));
				double HH05_= 0.5*(mu(r1+h1*i,h2*(s-1),0)*dqx(r1+h1*i,h2*(s-1),0,2,2)
                                   + mu(r1+h1*i,h2*s,0)*dqx(r1+h1*i,h2*s,0,2,2));
				double H305 = 0.5*(mu(r1+h1*i,h2*s,h3*1)*dqx(r1+h1*i,h2*s,h3*1,3,3)
                                   + mu(r1+h1*i,h2*s,0)*dqx(r1+h1*i,h2*s,0,3,3));
				double H305_= 0.5*(mu(r1+h1*i,h2*s,h3*(N3-1))*dqx(r1+h1*i,h2*s,h3*(N3-1),3,3)
                                   + mu(r1+h1*i,h2*s,0)*dqx(r1+h1*i,h2*s,0,3,3));
				ksi_1[i][s][0] = (-1.0)*(dqx(r1+h1*i,h2*s,0,1,1)*(derivative(kDerTypeVelocity,
                                                                             H(r1+h1*(i+1),j)*pow(v1[i+1][s][0],2),
                                                                             H(r1+h1*i,j)*pow(v1[i][s][0],2),
                                                                             H(r1+h1*(i-1),j)*pow(v1[i-1][s][0],2),
                                                                             1, i, s, 0)
                                 - 2.0*(Hmu0_5*(v1[i+1][s][0]-v1[i][s][0]) - Hmu_0_5*(v1[i][s][0]-v1[i-1][s][0]))/(h1*h1)
                                                                  )/H(r1+h1*i,j)
                                         + dqx(r1+i*h1,s*h2,0,1,1)*derivative(kDerTypePressure,
                                                                              p[i+1][s][0],
                                                                              p[i][s][0],
                                                                              p[i-1][s][0],
                                                                              1, i, s, 0)
                                         - j*(
                                        pow(v3[i][s][0],2) - 2.0*mu(r1+i*h1,s*h2,0)*v1[i][s][0]/H(r1+i*h1,j)
                                              )/H(r1+i*h1,j)
                                         + dqx(r1+h1*i,h2*s,0,2,2)*(derivative(kDerTypeVelocity,
                                                                               v1[i][s+1][0]*v2[i][s+1][0],
                                                                               v1[i][s][0]*v2[i][s][0],
                                                                               v1[i][s-1][0]*v2[i][s-1][0],
                                                                               2, i, s, 0)
            - (mu(r1+h1*i,h2*(s+1),0)*dqx(r1+h1*i,h2*(s+1),0,1,1)*(v2[i+1][s+1][0]-v2[i-1][s+1][0]) - mu(r1+h1*i,h2*(s-1),0)*dqx(r1+h1*i,h2*(s-1),0,1,1)*(v2[i+1][s-1][0]-v2[i-1][s-1][0]) )/(4.0*h1*h2)
            - (HH05*(v1[i][s+1][0]-v1[i][s][0]) - HH05_*(v1[i][s][0]-v1[i][s-1][0]))/(h2*h2)
                                                                    )
                                         + dqx(r1+h1*i,h2*s,0,3,3)*(derivative(kDerTypeVelocity,
                                                                               v1[i][s][1]*v3[i][s][1],
                                                                               v1[i][s][0]*v3[i][s][0],
                                                                               v1[i][s][N3-1]*v3[i][s][N3-1],
                                                                               3, i, s, 0)
                - (mu(r1+h1*i,h2*s,h3*1)*dqx(r1+h1*i,h2*s,h3*1,1,1)*(v3[i+1][s][1]-v3[i-1][s][1]) - mu(r1+h1*i,h2*s,h3*(N3-1))*dqx(r1+h1*i,h2*s,h3*(N3-1),1,1)*(v3[i+1][s][N3-1]-v3[i-1][s][N3-1])
                   )/(4.0*h1*h3)
                - (H305*(v1[i][s][1]-v1[i][s][0]) - H305_*(v1[i][s][0]-v1[i][s][N3-1]) )/(H(r1+h1*i,j)*h3*h3)
                + j*(3.0*mu(r1+i*h1,s*h2,0)*derivative(kDerTypeVelocity,
                                                       v3[i][s][1],
                                                       v3[i][s][0],
                                                       v3[i][s][N3-1],
                                                       3, i, s, 0)
                     + v3[i][s][0]*derivative(kDerTypeVelocity,
                                              mu(r1+i*h1,s*h2,1*h3),
                                              mu(r1+i*h1,s*h2,0),
                                              mu(r1+i*h1,s*h2,(N3-1)*h3),
                                              3, i, s, 0) )/H(r1+h1*i,j)
                                                                    )/H(r1+h1*i,j)
                                         );
				
                //std::cout <<"ksi "<< ksi_1[i][s][0] << "\n";
				
				double mudq22 = 0.5*(mu(r1+i*h1,(s+1)*h2,0)*dqx(r1+i*h1,(s+1)*h2,0,2,2) + mu(r1+i*h1,s*h2,0)*dqx(r1+i*h1,s*h2,0,2,2));
				double mudq22_ = 0.5*(mu(r1+i*h1,(s-1)*h2,0)*dqx(r1+i*h1,(s-1)*h2,0,2,2) + mu(r1+i*h1,s*h2,0)*dqx(r1+i*h1,s*h2,0,2,2));
				ksi_2[i][s][0]= (-1.0)*(dqx(r1+i*h1,s*h2,0,1,1)*(derivative(kDerTypeVelocity,
                                                                            v1[i+1][s][0]*v2[i+1][s][0]*H(r1+h1*(i+1),j),
                                                                            v1[i][s][0]*v2[i][s][0]*H(r1+h1*i,j),
                                                                            v1[i-1][s][0]*v2[i-1][s][0]*H(r1+h1*(i-1),j),
                                                                            1, i, s, 0)
                                                                 - (Hmu0_5*(v2[i+1][s][0]-v2[i][s][0]) - Hmu_0_5*(v2[i][s][0]-v2[i-1][s][0]))/(h1*h1)
                                                                 - dqx(r1+i*h1,s*h2,0,2,2)*(H(r1+(i+1)*h1,j)*mu(r1+(i+1)*h1,s*h2,0)*(v1[i+1][s+1][0] - v1[i+1][s-1][0]) - H(r1+(i-1)*h1,j)*mu(r1+(i-1)*h1,s*h2,0)*(v1[i-1][s+1][0] - v1[i-1][s-1][0]) )/(4.0*h1*h2)
                                                                 )/H(r1+h1*i,j)
                                        + dqx(r1+i*h1,s*h2,0,2,2)*((derivative(kDerTypeVelocity,
                                                                               pow(v2[i][s+1][0],2),
                                                                               pow(v2[i][s][0],2),
                                                                               pow(v2[i][s-1][0],2),
                                                                               2, i, s, 0)
                                                                    + derivative(kDerTypePressure,
                                                                                 p[i][s+1][0],
                                                                                 p[i][s][0],
                                                                                 p[i][s-1][0],
                                                                                 2, i, s, 0)
                                                                    )
               - 2.0*(mudq22*(v2[i][s+1][0]-v2[i][s][0]) - mudq22_*(v2[i][s][0]-v2[i][s-1][0]) )/(h2*h2)
               ) + dqx(r1+i*h1,s*h2,0,3,3)*(derivative(kDerTypeVelocity,
                                                       v2[i][s][1]*v3[i][s][1],
                                                       v2[i][s][0]*v3[i][s][0],
                                                       v2[i][s][N3-1]*v3[i][s][N3-1],
                                                       3, i, s, 0)
                                            - dqx(r1+i*h1,s*h2,0,2,2)*(mu(r1+i*h1,s*h2,1*h3)*(v3[i][s+1][1]-v3[i][s-1][1]) - mu(r1+i*h1,s*h2,(N3-1)*h3)*(v3[i][s+1][N3-1]-v3[i][s-1][N3-1]) )/(4.0*h2*h3)
                                            - ( H305*(v2[i][s][1]-v2[i][s][0]) - H305_*(v2[i][s][0]-v2[i][s][N3-1]) )/(H(r1+h1*i,j)*h3*h3)
                                                )/H(r1+i*h1,j));
                
				double HHmudq11 = 0.5*(pow(H(r1+(i+1)*h1,j),2)*mu(r1+(i+1)*h1,s*h2,0)*dqx(r1+(i+1)*h1,s*h2,0,1,1)
                                       + pow(H(r1+i*h1,j),2)*mu(r1+i*h1,s*h2,0)*dqx(r1+i*h1,s*h2,0,1,1) );
				double HHmudq11_ = 0.5*(pow(H(r1+(i-1)*h1,j),2)*mu(r1+(i-1)*h1,s*h2,0)*dqx(r1+(i-1)*h1,s*h2,0,1,1)
                                        + pow(H(r1+i*h1,j),2)*mu(r1+i*h1,s*h2,0)*dqx(r1+i*h1,s*h2,0,1,1) );
				ksi_3[i][s][0] = (-1.0)*(dqx(r1+i*h1,s*h2,0,1,1)*(derivative(kDerTypeVelocity,
                                                                             pow(H(r1+(i+1)*h1,j),2)*v1[i+1][s][0]*v3[i+1][s][0],
                                                                             pow(H(r1+i*h1,j),2)*v1[i][s][0]*v3[i][s][0],
                                                                             pow(H(r1+(i-1)*h1,j),2)*v1[i-1][s][0]*v3[i-1][s][0],
                                                                             1, i, s, 0)
              -(HHmudq11*(v3[i+1][s][0]-v3[i][s][0]) - HHmudq11_*(v3[i][s][0]-v3[i-1][s][0]) )/(h1*h1)
              - dqx(r1+i*h1,s*h2,0,3,3)*(H(r1+(i+1)*h1,j)*mu(r1+(i+1)*h1,s*h2,0)*(v1[i+1][s][1]-v1[i+1][s][N3-1]) - H(r1+(i-1)*h1,j)*mu(r1+(i-1)*h1,s*h2,0)*(v1[i-1][s][1]-v1[i-1][s][N3-1]) )/(4.0*h1*h3)
                                                                  )/pow(H(r1+i*h1,j),2)
                                         + dqx(r1+i*h1,s*h2,0,2,2)*(derivative(kDerTypeVelocity,
                                                                               v2[i][s+1][0]*v3[i][s+1][0],
                                                                               v2[i][s][0]*v3[i][s][0],
                                                                               v2[i][s-1][0]*v3[i][s-1][0],
                                                                               2, i, s, 0)
              - (mudq22*(v3[i][s+1][0]-v3[i][s][0]) - mudq22_*(v3[i][s][0]-v3[i][s-1][0]) )/(h2*h2)
              - dqx(r1+i*h1,s*h2,0,3,3)*(mu(r1+i*h1,(s+1)*h2,0)*(v2[i][s+1][1]-v2[i][s+1][N3-1]) - mu(r1+i*h1,(s-1)*h2,0)*(v2[i][s-1][1]-v2[i][s-1][N3-1]) )/(H(r1+i*h1,j)*4.0*h2*h3)
                                                    ) + dqx(r1+i*h1,s*h2,0,3,3)*((derivative(kDerTypeVelocity,
                                                                                             pow(v3[i][s][1],2),
                                                                                             pow(v3[i][s][0],2),
                                                                                             pow(v3[i][s][N3-1],2),
                                                                                             3, i, s, 0)
                                                                                  + derivative(kDerTypePressure,
                                                                                               p[i][s][1],
                                                                                               p[i][s][0],
                                                                                               p[i][s][N3-1],
                                                                                               3, i, s, 0)
                                                                                  )
                 - 2.0*( H305*(v3[i][s][1]-v3[i][s][0]) - H305_*(v3[i][s][0]-v3[i][s][N3-1]) )/(H(r1+h1*i,j)*h3*h3)
                 )/H(r1+i*h1,j) + j*(
                                     dqx(r1+i*h1,s*h2,0,1,1)*derivative(kDerTypeVelocity,
                                                                        mu(r1+(i+1)*h1,s*h2,0)*H(r1+(i+1)*h1,j)*v3[i+1][s][0],
                                                                        mu(r1+i*h1,s*h2,0)*H(r1+i*h1,j)*v3[i][s][0],
                                                                        mu(r1+(i-1)*h1,s*h2,0)*H(r1+(i-1)*h1,j)*v3[i-1][s][0],
                                                                        1, i, s, 0)
                                     - 2.0*dqx(r1+i*h1,s*h2,0,3,3)*derivative(kDerTypeVelocity,
                                                                              mu(r1+i*h1,s*h2,1*h3)*v1[i][s][1],
                                                                              mu(r1+i*h1,s*h2,0)*v1[i][s][0],
                                                                              mu(r1+i*h1,s*h2,(N3-1)*h3)*v1[i][s][N3-1],
                                                                              3, i, s, 0)
                                            )/pow(H(r1+i*h1,j),2));

				ksi_p[i][s][N3] = ksi_p[i][s][0];
				ksi_1[i][s][N3] = ksi_1[i][s][0];
				ksi_2[i][s][N3] = ksi_2[i][s][0];
				ksi_3[i][s][N3] = ksi_3[i][s][0];

			}
		}
	}//end if
	if(j==0){
        for(int i=0;i<=N1;i++)
        {
            for(int s=0;s<=N2;s++)
            {
                ksi_p[i][s][0] = 0;
                ksi_p[i][s][N3] = 0;
                ksi_1[i][s][0] = 0;
                ksi_1[i][s][N3] = 0;
                ksi_2[i][s][0] = 0;
                ksi_2[i][s][N3] = 0;
                ksi_3[i][s][0] = 0;
                ksi_3[i][s][N3] = 0;
            }
        }
	}
	for(int s=0;s<=N2;s++)
	{
		for(int k=0;k<=N3;k++)
		{
			ksi_p[0][s][k] = (4.0*ksi_p[1][s][k]-ksi_p[2][s][k])/3.0;;
			ksi_p[N1][s][k] = (4.0*ksi_p[N1-1][s][k]-ksi_p[N1-2][s][k])/3.0;;
			ksi_1[0][s][k] = 0;
			ksi_1[N1][s][k] = 0;
			ksi_2[0][s][k] = 0;
			ksi_2[N1][s][k] = 0;
			ksi_3[0][s][k] = 0;
			ksi_3[N1][s][k] = 0;
		}
	}
	
	for(int i=0;i<=N1;i++)
	{
		for(int k=0;k<=N3;k++)
		{
			ksi_p[i][0][k] = (4.0*ksi_p[i][1][k]-ksi_p[i][2][k])/3.0;
			ksi_p[i][N2][k] = (4.0*ksi_p[i][N2-1][k]-ksi_p[i][N2-2][k])/3.0;
			ksi_1[i][0][k] = 0;
			ksi_1[i][N2][k] = 0;
			ksi_2[i][0][k] = 0;
			ksi_2[i][N2][k] = 0;
			ksi_3[i][0][k] = 0;
			ksi_3[i][N2][k] = 0;
		}
	}
	
	
	double*** ksi_p_tmp = new double**[N1+1];

	for(int i=0;i<=N1;i++){
		ksi_p_tmp[i] = new double*[N2+1];
	}
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			ksi_p_tmp[i][s] = new double[N3+1];
		}
	}
	double*** ksi_0 = new double**[N1+1];

	for(int i=0;i<=N1;i++){
		ksi_0[i] = new double*[N2+1];
	}
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			ksi_0[i][s] = new double[N3+1];
		}
	}
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			for(int k=0;k<=N3;k++){
                ksi_0[i][s][k] = 0;//ksi_p[i][s][k];
			}
		}
	}
	
	double dist = 1;
	//puasson
	int t = 0;
    double range = 100;
	while(dist > eps){
		t++;
		if(t>OMEGA || range < epsRange)
			break;
		//1 step
	for(int i=1;i<N1;i++){
		for(int s=1;s<N2;s++){
			for(int k=0;k<=N3;k++){
				double f1,f2,f3;
				//if((k > 0 && k < N3) || j==1 ){
					f1 = dqx(r1+i*h1,s*h2,k*h3,1,1)*derivative(kDerTypePuasson,
                                                               H(r1+(i+1)*h1,j)*ksi_1[i+1][s][k],
                                                               H(r1+i*h1,j)*ksi_1[i][s][k],
                                                               H(r1+(i-1)*h1,j)*ksi_1[i-1][s][k],
                                                               1, i, s, k)/H(r1+i*h1,j);
					f2 =  dqx(r1+i*h1,s*h2,k*h3,2,2)*derivative(kDerTypePuasson,
                                                                ksi_2[i][s+1][k],
                                                                ksi_2[i][s][k],
                                                                ksi_2[i][s-1][k],
                                                                2, i, s, k);
					if(k == 0 || k == N3){
						f3 = dqx(r1+i*h1,s*h2,0,3,3)*derivative(kDerTypePuasson,
                                                                ksi_3[i][s][1],
                                                                ksi_3[i][s][0],
                                                                ksi_3[i][s][N3-1],
                                                                3, i, s, k);
					} else {
						f3 = dqx(r1+i*h1,s*h2,k*h3,3,3)*derivative(kDerTypePuasson,
                                                                   ksi_3[i][s][k+1],
                                                                   ksi_3[i][s][k],
                                                                   ksi_3[i][s][k-1],
                                                                   3, i, s, k);
					}

					ksi_p_tmp[i][s][k] = Lph(ksi_0,i,s,k) - (f1+f2+f3)/(tau*alpha) + ksi_p[i][s][k]/(pow(tau*alpha,2));
				//}else{
				//	std::cout << "not bad \n";
				//}
			}
			if(j==0){
                ksi_p_tmp[i][s][0] = 0;
                ksi_p_tmp[i][s][N3] = 0;
			}
		}
	}
	for(int s=0;s<=N2;s++){
		for(int k=0;k<=N3;k++){
			ksi_p_tmp[0][s][k] = (4.0*ksi_p_tmp[1][s][k]-ksi_p_tmp[2][s][k])/3.0;;
			ksi_p_tmp[N1][s][k] = (4.0*ksi_p_tmp[N1-1][s][k]-ksi_p_tmp[N1-2][s][k])/3.0;;
		}
	}

	for(int i=0;i<=N1;i++){
		for(int k=0;k<=N3;k++){
			ksi_p_tmp[i][0][k] = (4.0*ksi_p_tmp[i][1][k]-ksi_p_tmp[i][2][k])/3.0;
			ksi_p_tmp[i][N2][k] = (4.0*ksi_p_tmp[i][N2-1][k]-ksi_p_tmp[i][N2-2][k])/3.0;
		}
	}
	
	for(int s=0;s<=N2;s++){
		for(int k=0;k<=N3;k++){
			
			double* ap = new double[N1+1];
			double* bp = new double[N1+1];
			double* cp = new double[N1+1];
			double* dp = new double[N1+1];
			for(int i=1;i<N1;i++){
				double Hd = 0.5*(H(r1+(i+1)*h1,j)*dqx(r1+(i+1)*h1,s*h2,k*h3,1,1)
                                 + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1));
				double Hd_= 0.5*(H(r1+(i-1)*h1,j)*dqx(r1+(i-1)*h1,s*h2,k*h3,1,1)
                                 + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1));
							
				ap[i] = (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*Hd_)/(H(r1+i*h1,j)*h1*h1);
				bp[i] = (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*Hd)/(H(r1+i*h1,j)*h1*h1);
				cp[i] = 1 + (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*(Hd+Hd_))/(H(r1+i*h1,j)*h1*h1);
				dp[i] = ksi_p_tmp[i][s][k];
			}
			double* alp_ = new double[N1+1];
			double* beta_ = new double[N1+1];
			
			//alp_[1] = 1;
			//beta_[1] = 0;
			alp_[1]=(4*bp[1]- cp[1])/(3*bp[1] - ap[1]);
			beta_[1]=dp[1]/(3*bp[1] - ap[1]);
			
			for(int i=1;i<N1;i++){
				alp_[i+1] = bp[i]/(cp[i]-ap[i]*alp_[i]);
				beta_[i+1]= (dp[i] + ap[i]*beta_[i])/(cp[i]-ap[i]*alp_[i]);
			}
			
			ksi_p_tmp[N1][s][k] = ((4-alp_[N1-1])*beta_[N1] - beta_[N1-1])/(3 - (4.0- alp_[N1-1])*alp_[N1]);
			//ksi_p_tmp[N1][s][k] = beta_[N1]/(1-alp_[N1]);
			for(int i=N1-1;i>=0;i--){
				ksi_p_tmp[i][s][k]= alp_[i+1]*ksi_p_tmp[i+1][s][k] + beta_[i+1];
			}
			delete []ap;
			delete []bp;
			delete []cp;
			delete []dp;
			delete []alp_;
			delete []beta_;
		}
	}
	for(int i=0;i<=N1;i++){
		for(int k=0;k<=N3;k++){
			ksi_p_tmp[i][0][k] =  (4.0*ksi_p_tmp[i][1][k]-ksi_p_tmp[i][2][k])/3.0;
			ksi_p_tmp[i][N2][k] = (4.0*ksi_p_tmp[i][N2-1][k]-ksi_p_tmp[i][N2-2][k])/3.0;
		}
	}
	for(int i=1;i<N1;i++){
		for(int k=0;k<=N3;k++){
			double* ap = new double[N2+1];
			double* bp = new double[N2+1];
			double* cp = new double[N2+1];
			double* dp = new double[N2+1];
			for(int s=1;s<N2;s++){
				double Hd = 0.5*(dqx(r1+i*h1,(s+1)*h2,k*h3,2,2) + dqx(r1+i*h1,s*h2,k*h3,2,2));
				double Hd_= 0.5*(dqx(r1+i*h1,(s-1)*h2,k*h3,2,2) + dqx(r1+i*h1,s*h2,k*h3,2,2));
							
				ap[s] = (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*Hd_)/(h2*h2);
				bp[s] = (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*Hd)/(h2*h2);
				cp[s] = 1 + (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*(Hd+Hd_))/(h2*h2);
				dp[s] = ksi_p_tmp[i][s][k];
			}
			double* alp_ = new double[N2+1];
			double* beta_ = new double[N2+1];
			
			//alp_[1]=1;
			//beta_[1]=0;
			alp_[1]=(4*bp[1]- cp[1])/(3*bp[1] - ap[1]);
			beta_[1]=dp[1]/(3*bp[1] - ap[1]);
			
			for(int s=1;s<N2;s++){
				alp_[s+1] = bp[s]/(cp[s]-ap[s]*alp_[s]);
				beta_[s+1]= (dp[s] + ap[s]*beta_[s])/(cp[s]-ap[s]*alp_[s]);
			}

			ksi_p_tmp[i][N2][k] = ((4-alp_[N2-1])*beta_[N2] - beta_[N2-1])/(3 - (4.0- alp_[N2-1])*alp_[N2]);//
            //ksi_p_tmp[i][N2][k] = beta_[N2]/(1-alp_[N2]);
            
			for(int s = N2-1;s>=0;s--){
				ksi_p_tmp[i][s][k] = alp_[s+1]*ksi_p_tmp[i][s+1][k] + beta_[s+1];
			}

			delete []ap;
			delete []bp;
			delete []cp;
			delete []dp;
			delete []alp_;
			delete []beta_;
		}
	}
	for(int s=0;s<=N2;s++){
		for(int k=0;k<=N3;k++){
			ksi_p_tmp[N1][s][k] = (4.0*ksi_p_tmp[N1-1][s][k]-ksi_p_tmp[N1-2][s][k])/3.0;
			ksi_p_tmp[0][s][k] = (4.0*ksi_p_tmp[1][s][k]-ksi_p_tmp[2][s][k])/3.0;
		}
	}
	
	for(int i=1;i<N1;i++){
		for(int s=1;s<N2;s++){
			double* ap = new double[N3+1];
			double* bp = new double[N3+1];
			double* cp = new double[N3+1];
			double* dp = new double[N3+1];
			for(int k=1;k<N3;k++){
				double Hd = 0.5*(dqx(r1+i*h1,s*h2,(k+1)*h3,3,3) + dqx(r1+i*h1,s*h2,k*h3,3,3));
				double Hd_= 0.5*(dqx(r1+i*h1,s*h2,(k-1)*h3,3,3) + dqx(r1+i*h1,s*h2,k*h3,3,3));
							
				ap[k] = (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*Hd_)/(H(r1+i*h1,j)*H(r1+i*h1,j)*h3*h3);
				bp[k] = (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*Hd)/(H(r1+i*h1,j)*H(r1+i*h1,j)*h3*h3);
				cp[k] = 1 + (tau2*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*(Hd+Hd_))/(H(r1+i*h1,j)*H(r1+i*h1,j)*h3*h3);
				dp[k] = ksi_p_tmp[i][s][k];
			}
			double Hd = 0.5*(dqx(r1+i*h1,s*h2,h3,3,3) + dqx(r1+i*h1,s*h2,0,3,3));
			double Hd_= 0.5*(dqx(r1+i*h1,s*h2,(N3-1)*h3,3,3) + dqx(r1+i*h1,s*h2,N3*h3,3,3));
			
				ap[0] = (tau2*alpha*dqx(r1+i*h1,s*h2,0,3,3)*Hd_)/(H(r1+i*h1,j)*H(r1+i*h1,j)*h3*h3);
				bp[0] = (tau2*alpha*dqx(r1+i*h1,s*h2,0,3,3)*Hd)/(H(r1+i*h1,j)*H(r1+i*h1,j)*h3*h3);
				cp[0] = 1 + (tau2*alpha*dqx(r1+i*h1,s*h2,0,3,3)*(Hd+Hd_))/(H(r1+i*h1,j)*H(r1+i*h1,j)*h3*h3);
				dp[0] = ksi_p_tmp[i][s][0];
				
			double* alp_ = new double[N3+1];
			double* beta_ = new double[N3+1];
			double* gamma_ = new double[N3+1];
			alp_[1]=0;
			beta_[1]=0;
			gamma_[1]=1;
			for(int k=1;k<N3;k++){
				alp_[k+1] = bp[k]/(cp[k]-ap[k]*alp_[k]);
				beta_[k+1]= (dp[k] + ap[k]*beta_[k])/(cp[k]-ap[k]*alp_[k]);
				gamma_[k+1]= (ap[k]*gamma_[k])/(cp[k]-ap[k]*alp_[k]);	
				
			}
			double* V = new double[N3+1];
			double* U = new double[N3+1];
			U[0]=0;
			U[N3]=0;
			V[0]=1;
			V[N3]=1;
			for(int k=N3-1;k>=0;k--){
				U[k]= alp_[k+1]*U[k+1] + beta_[k+1];
				V[k]= alp_[k+1]*V[k+1] + gamma_[k+1];
			}
			double co;
			if(j==1){
			co = (dp[0] + ap[0]*beta_[N3] + bp[0]*U[1])/(cp[0] - ap[0]*alp_[N3] - ap[0]*gamma_[N3] - bp[0]*V[1]);
			//co = (dp[0] + ap[0]*U[N3-1] + bp[0]*U[1])/(cp[0] - ap[0]*V[N3-1] - bp[0]*V[1]);
			} else {
			co=0;
			}

			for(int k=0;k<=N3;k++){
				ksi_p_tmp[i][s][k]=  U[k] + co*V[k];
			}
			delete []ap;
			delete []bp;
			delete []cp;
			delete []dp;
			delete []alp_;
			delete []beta_;
			delete []gamma_;
			delete []V;
			delete []U;
		}
	}
	double max=0;
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			for(int k=0;k<=N3;k++){
				ksi_0[i][s][k] = ksi_0[i][s][k] + tau2*ksi_p_tmp[i][s][k];
				if(fabs(ksi_p_tmp[i][s][k])>max){
					max = fabs(ksi_p_tmp[i][s][k]);
				}
			}
		}
	}
        range = fabs(dist - max);
	dist = max;	
//std::cout << "\n"<< dist << "  " << range << "  " << t;
        std::cout << "\n"<< dist << "  " << t;
	for(int s=0;s<=N2;s++){
		for(int k=0;k<=N3;k++){
			ksi_0[N1][s][k] = (4.0*ksi_0[N1-1][s][k]-ksi_0[N1-2][s][k])/3.0;
			ksi_0[0][s][k] = (4.0*ksi_0[1][s][k]-ksi_0[2][s][k])/3.0;
		}
	}

	for(int i=0;i<=N1;i++){
		for(int k=0;k<=N3;k++){
			ksi_0[i][0][k] =  (4.0*ksi_0[i][1][k]-ksi_0[i][2][k])/3.0;
			ksi_0[i][N2][k] = (4.0*ksi_0[i][N2-1][k]-ksi_0[i][N2-2][k])/3.0;
		}
	}//*/
}//end while

for(int i=0;i<=N1;i++){
	for(int s=0;s<=N2;s++){
		for(int k=0;k<=N3;k++){
			ksi_p[i][s][k]=ksi_0[i][s][k];
		}
	}
}


double max = 0;
    int ii = 0;
    int ss = 0;
    int kk = 0;
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			for(int k=0;k<=N3;k++){
				if(max < fabs(ksi_p[i][s][k]))
				{
					max = fabs(ksi_p[i][s][k]);
                    ii = i;
                    ss = s;
                    kk = k;
				}
			}
		}
	}

param = max;
    std::cout << " UzEL \n" << ii << " " << ss << " " << kk;

	//ksi n1/2 || ksi p n1/2 = ksi p n+1
	for(int i=1;i<N1;i++){
		for(int s=1;s<N2;s++){
			for(int k=0;k<=N3;k++){
				ksi_1[i][s][k] = ksi_1[i][s][k] - tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*derivative(kDerTypePuassonEND,
                                                                                                  ksi_p[i+1][s][k],
                                                                                                  ksi_p[i][s][k],
                                                                                                  ksi_p[i-1][s][k],
                                                                                                  1, i, s, k);
			}
		}
	}
	for(int i=1;i<N1;i++){
		for(int s=1;s<N2;s++){
			for(int k=0;k<=N3;k++){
				ksi_2[i][s][k] = ksi_2[i][s][k] - tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*derivative(kDerTypePuassonEND,
                                                                                                  ksi_p[i][s+1][k],
                                                                                                  ksi_p[i][s][k],
                                                                                                  ksi_p[i][s-1][k],
                                                                                                  2, i, s, k);
			}
		}
	}
	for(int i=0;i<=N1;i++){
		for(int s=1;s<N2;s++){
			for(int k=1;k<N3;k++){
				ksi_3[i][s][k] = ksi_3[i][s][k] - tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*derivative(kDerTypePuassonEND,
                                                                                                  ksi_p[i][s][k+1],
                                                                                                  ksi_p[i][s][k],
                                                                                                  ksi_p[i][s][k-1],
                                                                                                  3, i, s, k)/H(r1+i*h1,j);
			}
		}
	}
	if(j==1){
		for(int i=0;i<=N1;i++){
			for(int s=1;s<N2;s++){
				ksi_3[i][s][0] = ksi_3[i][s][0] - tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*derivative(kDerTypePuassonEND,
                                                                                               ksi_p[i][s][1],
                                                                                               ksi_p[i][s][0],
                                                                                               ksi_p[i][s][N3-1],
                                                                                               3, i, s, 0)/H(r1+i*h1,j);
				ksi_3[i][s][N3] = ksi_3[i][s][0];
			}
		}
	}

	//2step//////////////////////////2step/////2step/////2step////////2step///////2step/////////2step/////2step
	for(int s=1;s<N2;s++){
		for(int k=0;k<=N3;k++){
			//first speed
			double* ap = new double[N1+1];
			double* bp = new double[N1+1];
			double* cp = new double[N1+1];
			double* dp = new double[N1+1];
			for(int i=1;i<N1;i++){
				double Hd = 0.5*(H(r1+(i+1)*h1,j)*dqx(r1+(i+1)*h1,s*h2,k*h3,1,1)*mu(r1+(i+1)*h1,s*h2,k*h3)
                                 + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_= 0.5*(H(r1+(i-1)*h1,j)*dqx(r1+(i-1)*h1,s*h2,k*h3,1,1)*mu(r1+(i-1)*h1,s*h2,k*h3)
                                 + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1)*mu(r1+i*h1,s*h2,k*h3));
							
				ap[i] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*(1.0/(2.0*h1) + kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_1,
                                                                                                    1,
                                                                                                    i, s, k))
                + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*Hd_)/(H(r1+i*h1,j)*h1*h1);
				bp[i] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*(1.0/(2.0*h1) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_1,
                                                                                                           1,
                                                                                                           i, s, k))
                + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*Hd)/(H(r1+i*h1,j)*h1*h1);

				cp[i] = 1 + j*tau*alpha*2*mu(r1+i*h1,s*h2,k*h3)/(H(r1+i*h1,j)*H(r1+i*h1,j))
                + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*(Hd+Hd_))/(H(r1+i*h1,j)*h1*h1)
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_1,
                                                                              1, i, s, k);
				dp[i] = ksi_1[i][s][k];
			}
			double* alp_ = new double[N1+1];
			double* beta_ = new double[N1+1];
			alp_[1]=0;
			beta_[1]=0;
			
			for(int i=1;i<N1;i++){
				alp_[i+1] = bp[i]/(cp[i]-ap[i]*alp_[i]);
				beta_[i+1]= (dp[i] + ap[i]*beta_[i])/(cp[i]-ap[i]*alp_[i]);
			}
			
			for(int i=N1-1;i>=0;i--){
				ksi_1[i][s][k]= alp_[i+1]*ksi_1[i+1][s][k] + beta_[i+1];
				
			}
			//second speed
			for(int i=1;i<N1;i++){
				double Hd = 0.5*(H(r1+(i+1)*h1,j)*dqx(r1+(i+1)*h1,s*h2,k*h3,1,1)*mu(r1+(i+1)*h1,s*h2,k*h3)
                                 + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_= 0.5*(H(r1+(i-1)*h1,j)*dqx(r1+(i-1)*h1,s*h2,k*h3,1,1)*mu(r1+(i-1)*h1,s*h2,k*h3)
                                 + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1)*mu(r1+i*h1,s*h2,k*h3));
				
                ap[i] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*(1.0/(2.0*h1) + kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_2,
                                                                                                    1,
                                                                                                    i, s, k))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*Hd_)/(H(r1+i*h1,j)*h1*h1);
				bp[i] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*(1.0/(2.0*h1) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_2,
                                                                                                           1,
                                                                                                           i, s, k))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*Hd)/(H(r1+i*h1,j)*h1*h1);
                
				cp[i] = 1 + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*(Hd+Hd_))/(H(r1+i*h1,j)*h1*h1)
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_2,
                                                                              1,
                                                                              i, s, k);
				dp[i] = ksi_2[i][s][k];
			}
			alp_[1]=0;
			beta_[1]=0;
			

			for(int i=1;i<N1;i++){
				alp_[i+1] = bp[i]/(cp[i]-ap[i]*alp_[i]);
				beta_[i+1]= (dp[i] + ap[i]*beta_[i])/(cp[i]-ap[i]*alp_[i]);
			}
			
			for(int i=N1-1;i>=0;i--){
				ksi_2[i][s][k]= alp_[i+1]*ksi_2[i+1][s][k] + beta_[i+1];
			}
			//third
			for(int i=1;i<N1;i++){
				double Hd = 0.5*(H(r1+(i+1)*h1,j)*dqx(r1+(i+1)*h1,s*h2,k*h3,1,1)*mu(r1+(i+1)*h1,s*h2,k*h3) + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_= 0.5*(H(r1+(i-1)*h1,j)*dqx(r1+(i-1)*h1,s*h2,k*h3,1,1)*mu(r1+(i-1)*h1,s*h2,k*h3) + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1)*mu(r1+i*h1,s*h2,k*h3));

				double PP = derivative(0,
                                       H(r1+(i+1)*h1,j)*mu(r1+h1*(i+1),s*h2,k*h3),
                                       H(r1+i*h1,j)*mu(r1+h1*i,s*h2,k*h3),
                                       H(r1+(i-1)*h1,j)*mu(r1+h1*(i-1),s*h2,k*h3),
                                       1, i, s, k);
                
                ap[i] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*(1.0/(2.0*h1) + kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_3,
                                                                                                    1,
                                                                                                    i, s, k))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*Hd_)/(H(r1+i*h1,j)*h1*h1);
				bp[i] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*(1.0/(2.0*h1) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_3,
                                                                                                           1,
                                                                                                           i, s, k))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*Hd)/(H(r1+i*h1,j)*h1*h1);
                
				cp[i] = 1 + j*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*PP/(H(r1+i*h1,j)*H(r1+i*h1,j))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*(Hd+Hd_))/(H(r1+i*h1,j)*h1*h1)
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,1,1)*v1[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_3,
                                                                              1,
                                                                              i, s, k);
				dp[i] = ksi_3[i][s][k];
			}
			alp_[1]=0;
			beta_[1]=0;
			for(int i=1;i<N1;i++){
				alp_[i+1] = bp[i]/(cp[i]-ap[i]*alp_[i]);
				beta_[i+1]= (dp[i] + ap[i]*beta_[i])/(cp[i]-ap[i]*alp_[i]);
			}
			
			for(int i=N1-1;i>=0;i--){
				ksi_3[i][s][k]= alp_[i+1]*ksi_3[i+1][s][k] + beta_[i+1];
			}
			delete []ap;
			delete []bp;
			delete []cp;
			delete []dp;
			delete []alp_;
			delete []beta_;
		}
	}
	//third step.
	for(int i=1;i<N1;i++){
		for(int k=0;k<=N3;k++){
			//first speed
			double* ap = new double[N2+1];
			double* bp = new double[N2+1];
			double* cp = new double[N2+1];
			double* dp = new double[N2+1];
			for(int s=1;s<N2;s++){
				double Hd = 0.5*(dqx(r1+i*h1,(s+1)*h2,k*h3,2,2)*mu(r1+i*h1,(s+1)*h2,k*h3)
                                 + dqx(r1+i*h1,s*h2,k*h3,2,2)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_= 0.5*(dqx(r1+i*h1,(s-1)*h2,k*h3,2,2)*mu(r1+i*h1,(s-1)*h2,k*h3)
                                 + dqx(r1+i*h1,s*h2,k*h3,2,2)*mu(r1+i*h1,s*h2,k*h3));
							
				ap[s] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*(1.0/(2.0*h2) + kDerTypeSteps*smoothCoef(false,
                                                                                                   ksi_1,
                                                                                                   2, i, s, k))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*Hd_)/(h2*h2);
				bp[s] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*(1.0/(2.0*h2) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_1,
                                                                                                           2, i, s, k))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*Hd)/(h2*h2);
                
				cp[s] = 1 + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*(Hd+Hd_))/(h2*h2)
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_1,
                                                                              2, i, s, k);
				dp[s] = ksi_1[i][s][k];
			}
			
			double* alp_ = new double[N2+1];
			double* beta_ = new double[N2+1];
			alp_[1]=0;
			beta_[1]=0;
			for(int s=1;s<N2;s++){
				alp_[s+1] = bp[s]/(cp[s]-ap[s]*alp_[s]);
				beta_[s+1]= (dp[s] + ap[s]*beta_[s])/(cp[s]-ap[s]*alp_[s]);				
			}
			
			for(int s=N2-1;s>=0;s--){
				ksi_1[i][s][k]= alp_[s+1]*ksi_1[i][s+1][k] + beta_[s+1];
			}

			//second speed
			for(int s=1;s<N2;s++){
				double Hd = 0.5*(dqx(r1+i*h1,(s+1)*h2,k*h3,2,2)*mu(r1+i*h1,(s+1)*h2,k*h3) + dqx(r1+i*h1,s*h2,k*h3,2,2)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_= 0.5*(dqx(r1+i*h1,(s-1)*h2,k*h3,2,2)*mu(r1+i*h1,(s-1)*h2,k*h3) + dqx(r1+i*h1,s*h2,k*h3,2,2)*mu(r1+i*h1,s*h2,k*h3));
							
				ap[s] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*(1.0/(2.0*h2) + kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_2,
                                                                                                    2, i, s, k))
                + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*Hd_)/(h2*h2);
				bp[s] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*(1.0/(2.0*h2) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_2,
                                                                                                           2, i, s, k))
                + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*Hd)/(h2*h2);
				cp[s] = 1 + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*(Hd+Hd_))/(h2*h2)
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_2,
                                                                              2, i, s, k);
				dp[s] = ksi_2[i][s][k];
			}
			alp_[1]=0;
			beta_[1]=0;
			for(int s=1;s<N2;s++){
				alp_[s+1] = bp[s]/(cp[s]-ap[s]*alp_[s]);
				beta_[s+1]= (dp[s] + ap[s]*beta_[s])/(cp[s]-ap[s]*alp_[s]);				
			}
			
			for(int s=N2-1;s>=0;s--){
				ksi_2[i][s][k]= alp_[s+1]*ksi_2[i][s+1][k] + beta_[s+1];
			}
			//third speed
			for(int s=1;s<N2;s++){
				double Hd = 0.5*(dqx(r1+i*h1,(s+1)*h2,k*h3,2,2)*mu(r1+i*h1,(s+1)*h2,k*h3) + dqx(r1+i*h1,s*h2,k*h3,2,2)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_= 0.5*(dqx(r1+i*h1,(s-1)*h2,k*h3,2,2)*mu(r1+i*h1,(s-1)*h2,k*h3) + dqx(r1+i*h1,s*h2,k*h3,2,2)*mu(r1+i*h1,s*h2,k*h3));
							
				ap[s] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*(1.0/(2.0*h2) + kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_3,
                                                                                                    2, i, s, k))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*Hd_)/(h2*h2);
				bp[s] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*(1.0/(2.0*h2) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_3,
                                                                                                           2, i, s, k))
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*Hd)/(h2*h2);
				cp[s] = 1 + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*(Hd+Hd_))/(h2*h2)
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,2,2)*v2[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_3,
                                                                              2, i, s, k);
				dp[s] = ksi_3[i][s][k];
			}
			alp_[1]=0;
			beta_[1]=0;
			for(int s=1;s<N2;s++){
				alp_[s+1] = bp[s]/(cp[s]-ap[s]*alp_[s]);
				beta_[s+1]= (dp[s] + ap[s]*beta_[s])/(cp[s]-ap[s]*alp_[s]);				
			}
			
			for(int s=N2-1;s>=0;s--){
				ksi_3[i][s][k]= alp_[s+1]*ksi_3[i][s+1][k] + beta_[s+1];

			}
			delete []ap;
			delete []bp;
			delete []cp;
			delete []dp;
			delete []alp_;
			delete []beta_;
		}
	}
	
	//4 step
	for(int i=1;i<N1;i++){
		for(int s=1;s<N2;s++){
			//first speed
            double* ap = new double[N3+1];
			double* bp = new double[N3+1];
			double* cp = new double[N3+1];
			double* dp = new double[N3+1];
			for(int k=1;k<N3;k++){
				double Hd = 0.5*(dqx(r1+i*h1,s*h2,(k+1)*h3,3,3)*mu(r1+i*h1,h2,(k+1)*h3) + dqx(r1+i*h1,s*h2,k*h3,3,3)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_ = 0.5*(dqx(r1+i*h1,s*h2,(k-1)*h3,3,3)*mu(r1+i*h1,h2,(k-1)*h3) + dqx(r1+i*h1,s*h2,k*h3,3,3)*mu(r1+i*h1,s*h2,k*h3));
							
				ap[k] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*(1.0/(2.0*h3) + kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_1,
                                                                                                    3, i, s, k))/H(r1+i*h1,j)
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*Hd_)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
				bp[k] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*(1.0/(2.0*h3) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_1,
                                                                                                           3, i, s, k))/H(r1+i*h1,j)
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*Hd)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
				cp[k] = 1 + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*(Hd+Hd_))/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j))
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_1,
                                                                              3, i, s, k)/H(r1+i*h1,j);
				dp[k] = ksi_1[i][s][k];
			}
            double Hd = 0.5*(dqx(r1+i*h1,s*h2,h3,3,3)*mu(r1+i*h1,h2,h3)
                             + dqx(r1+i*h1,s*h2,0,3,3)*mu(r1+i*h1,s*h2,0));
            double Hd_ = 0.5*(dqx(r1+i*h1,s*h2,(N3-1)*h3,3,3)*mu(r1+i*h1,h2,(N3-1)*h3)
                              + dqx(r1+i*h1,s*h2,0,3,3)*mu(r1+i*h1,s*h2,0));
                        
            ap[0] = tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*(1.0/(2.0*h3) + kDerTypeSteps*smoothCoef(false,
                                                                                             ksi_1,
                                                                                             3, i, s, 0))/H(r1+i*h1,j)
            + (tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*Hd_)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
            bp[0] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*(1.0/(2.0*h3) - kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_1,
                                                                                                    3, i, s, 0))/H(r1+i*h1,j)
            + (tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*Hd)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
            cp[0] = 1 + (tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*(Hd+Hd_))/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j))
            + tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*kDerTypeSteps*smoothCoef(true,
                                                                       ksi_1,
                                                                       3, i, s, 0)/H(r1+i*h1,j);
            dp[0] = ksi_1[i][s][0];

			double* alp_ = new double[N3+1];
			double* beta_ = new double[N3+1];
			double* gamma_ = new double[N3+1];
			alp_[1]=0;
			gamma_[1]=1;
			beta_[1]=0;
			for(int k=1;k<N3;k++){
				alp_[k+1] = bp[k]/(cp[k]-ap[k]*alp_[k]);
				beta_[k+1]= (dp[k] + ap[k]*beta_[k])/(cp[k]-ap[k]*alp_[k]);
				gamma_[k+1]= (ap[k]*gamma_[k])/(cp[k]-ap[k]*alp_[k]);	
			}
			double* V = new double[N3+1];
			double* U = new double[N3+1];
			U[0]=0;
			U[N3]=0;
			V[0]=1;
			V[N3]=1;
			for(int k=N3-1;k>=0;k--){
				U[k]= alp_[k+1]*U[k+1] + beta_[k+1];
				V[k]= alp_[k+1]*V[k+1] + gamma_[k+1];
			}
			double co;
			if(j==1){
			co = (dp[0] + ap[0]*beta_[N3] + bp[0]*U[1])/(cp[0] - ap[0]*alp_[N3] - ap[0]*gamma_[N3] - bp[0]*V[1]);
			}
			else{
			co=0;
			}

			for(int k=0;k<=N3;k++){
				ksi_1[i][s][k]=  U[k] + co*V[k];

			}
			//second speed
            for(int k=1;k<N3;k++){
				double Hd = 0.5*(dqx(r1+i*h1,s*h2,(k+1)*h3,3,3)*mu(r1+i*h1,h2,(k+1)*h3) + dqx(r1+i*h1,s*h2,k*h3,3,3)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_ = 0.5*(dqx(r1+i*h1,s*h2,(k-1)*h3,3,3)*mu(r1+i*h1,h2,(k-1)*h3) + dqx(r1+i*h1,s*h2,k*h3,3,3)*mu(r1+i*h1,s*h2,k*h3));
                
				ap[k] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*(1.0/(2.0*h3) + kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_2,
                                                                                                    3, i, s, k))/H(r1+i*h1,j)
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*Hd_)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
				bp[k] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*(1.0/(2.0*h3) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_2,
                                                                                                           3, i, s, k))/H(r1+i*h1,j)
                + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*Hd)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
				cp[k] = 1 + (tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*(Hd+Hd_))/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j))
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_2,
                                                                              3, i, s, k)/H(r1+i*h1,j);
				dp[k] = ksi_2[i][s][k];
			}
    
            ap[0] = tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*(1.0/(2.0*h3) + kDerTypeSteps*smoothCoef(false,
                                                                                             ksi_2,
                                                                                             3, i, s, 0))/H(r1+i*h1,j)
            + (tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*Hd_)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
            bp[0] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*(1.0/(2.0*h3) - kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_2,
                                                                                                    3, i, s, 0))/H(r1+i*h1,j)
            + (tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*Hd)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
            cp[0] = 1 + (tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*(Hd+Hd_))/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j))
            + tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*kDerTypeSteps*smoothCoef(true,
                                                                       ksi_2,
                                                                       3, i, s, 0)/H(r1+i*h1,j);
            dp[0] = ksi_2[i][s][0];
			for(int k=1;k<N3;k++){
				dp[k]= ksi_2[i][s][k];
				alp_[k+1] = bp[k]/(cp[k]-ap[k]*alp_[k]);
				beta_[k+1]= (dp[k] + ap[k]*beta_[k])/(cp[k]-ap[k]*alp_[k]);
				gamma_[k+1]= (ap[k]*gamma_[k])/(cp[k]-ap[k]*alp_[k]);
			}

			for(int k=N3-1;k>=0;k--){
				U[k]= alp_[k+1]*U[k+1] + beta_[k+1];
				V[k]= alp_[k+1]*V[k+1] + gamma_[k+1];
			}
			if(j==1){
			co = (dp[0] + ap[0]*beta_[N3] + bp[0]*U[1])/(cp[0] - ap[0]*alp_[N3] - ap[0]*gamma_[N3] - bp[0]*V[1]);
			}
			else{
			co=0;
			}
			for(int k=0;k<=N3;k++){
				ksi_2[i][s][k]=  U[k] + co*V[k];
			}
			//third speed
			for(int k=1;k<N3;k++){
				double Hd = 0.5*(dqx(r1+i*h1,s*h2,(k+1)*h3,3,3)*mu(r1+i*h1,h2,(k+1)*h3) + dqx(r1+i*h1,s*h2,k*h3,3,3)*mu(r1+i*h1,s*h2,k*h3));
				double Hd_ = 0.5*(dqx(r1+i*h1,s*h2,(k-1)*h3,3,3)*mu(r1+i*h1,h2,(k-1)*h3) + dqx(r1+i*h1,s*h2,k*h3,3,3)*mu(r1+i*h1,s*h2,k*h3));
							
				ap[k] = tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*(1.0/(2.0*h3) + kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_3,
                                                                                                    3, i, s, k))/H(r1+i*h1,j)
                + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*Hd_)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
				bp[k] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*(1.0/(2.0*h3) - kDerTypeSteps*smoothCoef(false,
                                                                                                           ksi_3,
                                                                                                           3, i, s, k))/H(r1+i*h1,j)
                + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*Hd)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
				cp[k] = 1 + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*(Hd+Hd_))/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j))
                + tau*alpha*dqx(r1+i*h1,s*h2,k*h3,3,3)*v3[i][s][k]*kDerTypeSteps*smoothCoef(true,
                                                                              ksi_3,
                                                                              3, i, s, k)/H(r1+i*h1,j);
				dp[k] = ksi_3[i][s][k];				
			}

			Hd = 0.5*(dqx(r1+i*h1,s*h2,h3,3,3)*mu(r1+i*h1,h2,h3)
                      + dqx(r1+i*h1,s*h2,0,3,3)*mu(r1+i*h1,s*h2,0));
			Hd_ = 0.5*(dqx(r1+i*h1,s*h2,(N3-1)*h3,3,3)*mu(r1+i*h1,h2,(N3-1)*h3)
                       + dqx(r1+i*h1,s*h2,0,3,3)*mu(r1+i*h1,s*h2,0));
						
			ap[0] = tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*(1.0/(2.0*h3) + kDerTypeSteps*smoothCoef(false,
                                                                                             ksi_3,
                                                                                             3, i, s, 0))/H(r1+i*h1,j)
            + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*Hd_)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
			bp[0] = (-1.0)*tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*(1.0/(2.0*h3) - kDerTypeSteps*smoothCoef(false,
                                                                                                    ksi_3,
                                                                                                    3, i, s, 0))/H(r1+i*h1,j)
            + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*Hd)/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j));
			cp[0] = 1 + (2.0*tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*(Hd+Hd_))/(h3*h3*H(r1+i*h1,j)*H(r1+i*h1,j))
            + tau*alpha*dqx(r1+i*h1,s*h2,0,3,3)*v3[i][s][0]*kDerTypeSteps*smoothCoef(true,
                                                                       ksi_3,
                                                                       3, i, s, 0)/H(r1+i*h1,j);
			dp[0] = ksi_3[i][s][0];

			alp_[1]=0;
			beta_[1]=0;
			for(int k=1;k<N3;k++){
				alp_[k+1] = bp[k]/(cp[k]-ap[k]*alp_[k]);
				beta_[k+1]= (dp[k] + ap[k]*beta_[k])/(cp[k]-ap[k]*alp_[k]);
				gamma_[k+1]= (ap[k]*gamma_[k])/(cp[k]-ap[k]*alp_[k]);
			}
			for(int k=N3-1;k>=0;k--){
				U[k]= alp_[k+1]*U[k+1] + beta_[k+1];
				V[k]= alp_[k+1]*V[k+1] + gamma_[k+1];
			}
			if(j==1){
				co = (dp[0] + ap[0]*beta_[N3] + bp[0]*U[1])/(cp[0] - ap[0]*alp_[N3] - ap[0]*gamma_[N3] - bp[0]*V[1]);
			}
			else{
				co=0;
			}
			for(int k=0;k<=N3;k++){
				ksi_3[i][s][k]=  U[k] + co*V[k];
			}
			delete []ap;
			delete []bp;
			delete []cp;
			delete []dp;
			delete []alp_;
			delete []beta_;
			delete []gamma_;
			delete []V;
			delete []U;
		}
	}
	
	//the last step
	for(int i=1;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			for(int k=0;k<=N3;k++){
				ksi_3[i][s][k] = (ksi_3[i][s][k] - (j*(tau*alpha)*v3[i][s][k]*ksi_1[i][s][k])/H(r1+i*h1,j)
					)/(1 + j*(tau*alpha)*(v1[i][s][k] + (2.0*tau*alpha*pow(v3[i][s][k],2))/H(r1+i*h1,j))/H(r1+i*h1,j) );
				ksi_1[i][s][k] = ksi_1[i][s][k] + 2.0*j*v3[i][s][k]*tau*alpha*ksi_3[i][s][k]/H(r1+i*h1,j);
			}
		}
	}
	
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			for(int k=0;k<=N3;k++){
			p[i][s][k] = p[i][s][k] + tau*ksi_p[i][s][k];
			v1[i][s][k] = v1[i][s][k] + tau*ksi_1[i][s][k];
			v2[i][s][k] = v2[i][s][k] + tau*ksi_2[i][s][k];
			v3[i][s][k] = v3[i][s][k] + tau*ksi_3[i][s][k];
			}
		}
	}
	
	double mmax = 0;

	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
			for(int k=0;k<=N3;k++){
				if(fabs(ksi_1[i][s][k]) > mmax){
				mmax = fabs(ksi_1[i][s][k]);			
				}
				if(fabs(ksi_2[i][s][k]) > mmax){
				mmax = fabs(ksi_2[i][s][k]);			
				}
				if(fabs(ksi_3[i][s][k]) > mmax){
				mmax = fabs(ksi_3[i][s][k]);			
				}
			}
		}
	}
	if(param<mmax)
		param = mmax;
	
	
for(int i=0;i<=N1;i++){
	for(int s=0;s<=N2;s++){
		delete []ksi_p[i][s];
		delete []ksi_1[i][s];
		delete []ksi_2[i][s];
		delete []ksi_3[i][s];
		delete []ksi_p_tmp[i][s];
		delete []ksi_0[i][s];
	}
}
for(int i=0;i<=N1;i++){
	delete []ksi_p[i];
	delete []ksi_1[i];
	delete []ksi_2[i];
	delete []ksi_3[i];
	delete []ksi_p_tmp[i];
	delete []ksi_0[i];
}
delete []ksi_p;
delete []ksi_1;
delete []ksi_2;
delete []ksi_3;
delete []ksi_0;
delete []ksi_p_tmp;

return param;
}

double reac::Lph(double ***ksi,int i,int s,int k)
{
	double Hmu0_5 = 0.5*(H(r1+(i+1)*h1,j)*dqx(r1+(i+1)*h1,s*h2,k*h3,1,1)
                           + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1));
	double Hmu_0_5= 0.5*(H(r1+(i-1)*h1,j)*dqx(r1+(i-1)*h1,s*h2,k*h3,1,1)
                           + H(r1+i*h1,j)*dqx(r1+i*h1,s*h2,k*h3,1,1));
				
	double p1 = dqx(r1+i*h1,s*h2,k*h3,1,1)*(Hmu0_5*(ksi[i+1][s][k] - ksi[i][s][k])
                                            - Hmu_0_5*(ksi[i][s][k] - ksi[i-1][s][k])
                                            )/(H(r1+i*h1,j)*h1*h1);
	double p2;
	double p3;
	
	p2 = dqx(r1+i*h1,s*h2,k*h3,2,2)*0.5*((dqx(r1+i*h1,s*h2,k*h3,2,2) + dqx(r1+i*h1,(s+1)*h2,k*h3,2,2))*(ksi[i][s+1][k] - ksi[i][s][k])
                                         - (dqx(r1+i*h1,s*h2,k*h3,2,2) + dqx(r1+i*h1,(s-1)*h2,k*h3,2,2))*
        (ksi[i][s][k] - ksi[i][s-1][k])
                                         )/(h2*h2);
	if((k == 0 || k == N3)){
	p3 = dqx(r1+i*h1,s*h2,0,3,3)*0.5*((dqx(r1+i*h1,s*h2,h3,3,3) + dqx(r1+i*h1,s*h2,0,3,3))
                                      *(ksi[i][s][1] - ksi[i][s][0])
                                      - (dqx(r1+i*h1,s*h2,0,3,3) + dqx(r1+i*h1,s*h2,(N3-1)*h3,3,3))
                                      *(ksi[i][s][0] - ksi[i][s][N3-1])
                                      )/(H(r1+i*h1,j)*H(r1+i*h1,j)*h3*h3);
	} else {
	p3 = dqx(r1+i*h1,s*h2,k*h3,3,3)*0.5*((dqx(r1+i*h1,s*h2,(k+1)*h3,3,3) + dqx(r1+i*h1,s*h2,k*h3,3,3))
                                         *(ksi[i][s][k+1] - ksi[i][s][k])
                                         - (dqx(r1+i*h1,s*h2,k*h3,3,3) + dqx(r1+i*h1,s*h2,(k-1)*h3,3,3))
                                         *(ksi[i][s][k] - ksi[i][s][k-1])
                                         )/(H(r1+i*h1,j)*H(r1+i*h1,j)*h3*h3);
	}
	return p1+p2+p3;
}

double reac::get(int num,int i,int s,int k){
	if(num==1){
		return v1[i][s][k];
	}
	if(num==2){
		return v2[i][s][k];
	}
	if(num==3){
		return v3[i][s][k];
	}
	else{
		return p[i][s][k];
	}
}
double reac::get_eps(int num){
	if(num==1){
		return eps1;
	}
	if(num==2){
		return eps2;
	}
	if(num==3){
		return eps3;
	}
	else{
		return epsp;
	}
}
void reac::stream_function(double tok)
{
	double** psi = new double*[N1+1];
	double** psi_0 = new double*[N1+1];
	double** dzt = new double*[N1+1];
	for(int i=0;i<=N1;i++){
		psi[i] = new double[N2+1];
		psi_0[i] = new double[N2+1];
		dzt[i] = new double[N2+1];
	}
	
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
				psi[i][s]=0;
				psi_0[i][s]=psi[i][s];
		}
	}

	
	double dist =1;
	while(dist>0.000001){
		
	for(int i=1;i<N1;i++){
		for(int s=1;s<N2;s++){
			double f1,f2,l1,l2;
			double Hd = 0.5*(dqx(r1+(i+1)*h1,s*h2,0,1,1)/H(r1+(i+1)*h1,j) + dqx(r1+i*h1,s*h2,0,1,1)/H(r1+i*h1,j));
			double Hd_= 0.5*(dqx(r1+(i-1)*h1,s*h2,0,1,1)/H(r1+(i-1)*h1,j) + dqx(r1+i*h1,s*h2,0,1,1)/H(r1+i*h1,j));
			
			l1 = dqx(r1+i*h1,s*h2,0,1,1)*(Hd*(psi_0[i+1][s]-psi_0[i][s]) - Hd_*(psi_0[i][s] - psi_0[i-1][s]) )/(h1*h1);
			
			Hd = 0.5*(dqx(r1+i*h1,(s+1)*h2,0,2,2) + dqx(r1+i*h1,s*h2,0,2,2));
			Hd_= 0.5*(dqx(r1+i*h1,(s-1)*h2,0,2,2) + dqx(r1+i*h1,s*h2,0,2,2));
			
			l2 =  dqx(r1+i*h1,s*h2,0,2,2)*(Hd*(psi_0[i][s+1]-psi_0[i][s]) - Hd_*(psi_0[i][s] - psi_0[i][s-1]) )/(H(r1+i*h1,j)*h2*h2); 
			 
			f1 = (v2[i+1][s][0]-v2[i-1][s][0])/(2*h1);
			
			f2 = (v1[i][s+1][0]-v1[i][s-1][0])/(2*h2);

			dzt[i][s]= l1 +l2 +f1-f2;
		}
	}

		for(int s=1;s<N2;s++){	
			double* ap = new double[N1+1];
			double* bp = new double[N1+1];
			double* cp = new double[N1+1];
			double* dp = new double[N1+1];
			
			for(int i=1;i<N1;i++){
				double Hd = 0.5*(dqx(r1+(i+1)*h1,s*h2,0,1,1)/H(r1+(i+1)*h1,j) + dqx(r1+i*h1,s*h2,0,1,1)/H(r1+i*h1,j));
				double Hd_= 0.5*(dqx(r1+(i-1)*h1,s*h2,0,1,1)/H(r1+(i-1)*h1,j) + dqx(r1+i*h1,s*h2,0,1,1)/H(r1+i*h1,j));
							
				ap[i] = (tok*alpha*dqx(r1+i*h1,s*h2,0,1,1)*Hd_)/(h1*h1);
				bp[i] = (tok*alpha*dqx(r1+i*h1,s*h2,0,1,1)*Hd)/(h1*h1);
				cp[i] = 1 + (tok*alpha*dqx(r1+i*h1,s*h2,0,1,1)*(Hd+Hd_))/(h1*h1);
				dp[i] = dzt[i][s];
			}
			double* alp_ = new double[N1+1];
			double* beta_ = new double[N1+1];
			alp_[1]=1;
			beta_[1]=0;
		
			for(int i=1;i<N1;i++){
				alp_[i+1] = bp[i]/(cp[i]-ap[i]*alp_[i]);
				beta_[i+1]= (dp[i] + ap[i]*beta_[i])/(cp[i]-ap[i]*alp_[i]);
			}
			
			dzt[N1][s] = beta_[N1]/(1-alp_[N1]);
			
			for(int i=N1-1;i>=0;i--){
				dzt[i][s]= alp_[i+1]*dzt[i+1][s] + beta_[i+1];
			}
			delete []ap;
			delete []bp;
			delete []cp;
			delete []dp;
			delete []alp_;
			delete []beta_;
		
		}
		
		for(int i=1;i<N1;i++){
			double* ap = new double[N2+1];
			double* bp = new double[N2+1];
			double* cp = new double[N2+1];
			double* dp = new double[N2+1];
			for(int s=1;s<N2;s++){
				double Hd = 0.5*(dqx(r1+i*h1,(s+1)*h2,0,2,2) + dqx(r1+i*h1,s*h2,0,2,2));
				double Hd_= 0.5*(dqx(r1+i*h1,(s-1)*h2,0,2,2) + dqx(r1+i*h1,s*h2,0,2,2));
							
				ap[s] = (tok*alpha*dqx(r1+i*h1,s*h2,0,2,2)*Hd_)/(h2*h2*H(r1+i*h1,j));
				bp[s] = (tok*alpha*dqx(r1+i*h1,s*h2,0,2,2)*Hd)/(h2*h2*H(r1+i*h1,j));
				cp[s] = 1 + (tok*alpha*dqx(r1+i*h1,s*h2,0,2,2)*(Hd+Hd_))/(h2*h2*H(r1+i*h1,j));
				dp[s] = dzt[i][s];
			}
			double* alp_ = new double[N2+1];
			double* beta_ = new double[N2+1];
			
			alp_[1]=1;
			beta_[1]=0;			
			
			for(int s=1;s<N2;s++){
				alp_[s+1] = bp[s]/(cp[s]-ap[s]*alp_[s]);
				beta_[s+1]= (dp[s] + ap[s]*beta_[s])/(cp[s]-ap[s]*alp_[s]);
			}

			dzt[i][N2] = beta_[N2]/(1-alp_[N2]);

			for(int s = N2-1;s>=0;s--){
				dzt[i][s] = alp_[s+1]*dzt[i][s+1] + beta_[s+1];
			}
			delete []ap;
			delete []bp;
			delete []cp;
			delete []dp;
			delete []alp_;
			delete []beta_;
		}
		double max=0;
		for(int i=1;i<N1;i++){
			for(int s=1;s<N2;s++){
					psi_0[i][s] = psi_0[i][s] + tok*dzt[i][s];
					if(fabs(dzt[i][s])>max){
						max = fabs(dzt[i][s]);
				}
			}
		}
		dist = max;	
		
		std::cout << "\n"<< dist << "  PSI_FUNC";	

		for(int s=0;s<=N2;s++){
				psi_0[N1][s] = (4.0*psi_0[N1-1][s]-psi_0[N1-2][s])/3.0 - 2*h1*H(r1+N1*h1,j)*v2[N1][s][0];
				psi_0[0][s] =  (4.0*psi_0[1][s]-psi_0[2][s])/3.0 + 2*h1*H(r1,j)*v2[0][s][0];
		}

		for(int i=0;i<=N1;i++){
				psi_0[i][0] =  (4.0*psi_0[i][1]-psi_0[i][2])/3.0 - 2*h2*H(r1+i*h1,j)*v1[i][0][0];
				psi_0[i][N2] = (4.0*psi_0[i][N2-1]-psi_0[i][N2-2])/3.0 + 2*h2*H(r1+i*h1,j)*v1[i][N2][0];
		}

	}//end while

	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
				psi[i][s]=psi_0[i][s];
		}
	}
	std::ofstream FILEpsi("master/psi.nb");
	FILEpsi << "ListPlot3D[{" << "\n";
	for(int i=0;i<=N1;i++){
		for(int s=0;s<=N2;s++){
            std::string str = s==N2 && i==N1 ? "}" : "}, ";
			FILEpsi << "{" << 1+i*h1 <<", " << s*h2 <<", " << psi[i][s] << str << "\n";
		}
		FILEpsi << "\n";
	}
    FILEpsi << "}, Mesh -> 50, MeshFunctions -> {#3 &}, Lighting -> {White}, \nBoxRatios -> {1, 2, 0.3}]";
	FILEpsi.close();
	for(int i=0;i<=N1;i++){
		delete[] dzt[i];
		delete[] psi[i];
		delete[] psi_0[i];
	}
	delete[] dzt;
	delete[] psi;
	delete[] psi_0;
}