//
//  main.cpp
//  reac
//
//  Created by Anton Kudryashov on 29.03.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "reac.h"
#include <math.h>
#include <iostream>
#include <fstream>

#define pi 3.14159265

int main_old(int argc, const char * argv[])
{
    double h1 = 0.01;
	double h2 = 0.01;
	double h3 = pi;
	double alpha = 0.6;
	double tau = 0.05;
	reac obj(h1,h2,h3,tau,1,alpha);
	int kk = 0;
	
	std::ofstream FILE0("master/ksi_p.nb");
	std::ofstream FILEout1("master/outs1.nb");
	std::ofstream FILEout2("master/outs2.nb");
	std::ofstream FILEout3("master/outs3.nb");
	std::ofstream FILEoutp("master/outp.nb");
	double param = 10;
	int k=0;
    FILE0 << "ListPlot[{\n";
	while(fabs(param) > 0.0001 && k<= 900){
		std::cout << "iteration number[" << k << "]";
		param = obj.RS(kk);
		if(kk%10 == 0 && k > 0){
			//FILEepss << k << " v1: ";
			FILE0 << "{" << k << ", " << param << "}, ";
			//FILEepss << obj.get_eps(1) << " v2: " << obj.get_eps(2) << " v3: " << obj.get_eps(3) << " p: " << obj.get_eps(4) << "\n";
		}
		kk++;
		
		std::cout <<"\n  outside eps: " << param << " at["<<k<<"]\n";
		k++;
	}
    FILE0 << "{" << k << ", " << param << "}}]\n";
	double R1 = 1;
	double R2 = 2;
	double G = 2;
	int n1 = (int)((R2-R1)/h1);
	int n2 = (int)(G/h2);
	//int n3 = (int)(2.0*pi/h3);
    for(int s=0;s<=n2;s++){
        FILEout1 << "ListLinePlot[{\n";
        FILEout2 << "ListLinePlot[{\n";
        FILEout3 << "ListLinePlot[{\n";
        FILEoutp << "ListLinePlot[{\n";
        for(int i=0;i<=n1;i++){
            std::string str = i==n1 ? "}\n" : "}, \n";
            FILEout1 << "{" << i*h1 << ", "<< obj.get(1,i,s,0) << str;
            FILEout2 << "{" << i*h1 << ", "<< obj.get(2,i,s,0) << str;
            FILEout3 << "{" << i*h1 << ", "<< obj.get(3,i,s,0) << str;
            FILEoutp << "{" << i*h1 << ", "<< obj.get(5,i,s,0) << str;
		}
		FILEout1 << "}]\n";
		FILEout2 << "}]\n";
		FILEout3 << "}]\n";
		FILEoutp << "}]\n";
	}
	FILE0.close();
	FILEout1.close();
	FILEout2.close();
	FILEout3.close();
	FILEoutp.close();
    
	obj.stream_function(1);
    
    return 0;
}

