//
//  problem.cpp
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "problem.h"
#include "constants.h"
#include <iostream>
#include <math.h>
#include "arrargument.h"
#include "funcargument.h"

double f_p(double x1, double x2, double x3);
double f_v1(double x1, double x2, double x3);
double f_v2(double x1, double x2, double x3);
double f_v3(double x1, double x2, double x3);

problem::problem()
{
    tau = _tau;
    tau2 = _tau2;
    alpha = _alpha;
    
    v1 = new double**[mesh.getN(1)+1];
    v2 = new double**[mesh.getN(1)+1];
    v3 = new double**[mesh.getN(1)+1];
    p = new double**[mesh.getN(1)+1];
    for(int i=0;i<=mesh.getN(1);i++){
        v1[i] = new double*[mesh.getN(2)+1];
        v2[i] = new double*[mesh.getN(2)+1];
        v3[i] = new double*[mesh.getN(2)+1];
        p[i] = new double*[mesh.getN(2)+1];
    }
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            v1[i][s] = new double[mesh.getN(3)+1];
            v2[i][s] = new double[mesh.getN(3)+1];
            v3[i][s] = new double[mesh.getN(3)+1];
            p[i][s] = new double[mesh.getN(3)+1];
        }
    }
    funcargument V1(&f_v1, &mesh), V2(&f_v2, &mesh), V3(&f_v3, &mesh), P(&f_p, &mesh);
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                int* indx = mesh.indx(i, s, k);
                v1[i][s][k]=V1[indx];
                v2[i][s][k]=V2[indx];
                v3[i][s][k]=V3[indx];
                p[i][s][k]=P[indx];
            }
        }
    }
    f = new double***[4];
    f[0] = p;
    f[1] = v1;
    f[2] = v2;
    f[3] = v3;
    updatePressureBounds(p);
    std::cout << "Задача поставлена \n";
}

void problem::updatePressureBounds(double*** P)
{
    for (int direction = 1; direction<=3; direction++){
        bool* bounds = mesh.getPbounds(direction);
        updateBounds(P, direction, bounds);
        delete [] bounds;
    }
}

void problem::updateBounds(double ***F,
                           int direction,
                           bool bounds[2])
{
    return updateBounds(F, direction, bounds, false);
}

void problem::updateBounds(double ***F,
                           int direction,
                           bool bounds[2],
                           bool secondOrder)
{
    if (!bounds || !mesh.directionIsCorrect(direction) || (mesh.getJ() == 1 && direction == 3))
        return;
    arrargument arg(F);
    int* otherDirections = mesh.otherDirections(direction);
    int firstIndex = 1 - mesh.getJ();
    int N = mesh.getN(direction);
    for(int s=1;s<=mesh.getN(otherDirections[0])-1;s++){
        for(int k=firstIndex;k<=mesh.getN(otherDirections[1])-firstIndex;k++){
            if (bounds[0]){
                int* firstIndx = mesh.firstIndx(s, k, direction);
                int* nextIndx = mesh.nextIndx(firstIndx, direction);
                int* twoNextIndx = mesh.nextIndx(nextIndx, direction);
                F[firstIndx[0]][firstIndx[1]][firstIndx[2]] = secondOrder && N > 2 ? (4.0*arg[nextIndx] - arg[twoNextIndx])/3.0 : arg[nextIndx];
            }
            if (bounds[1]){
                int* lastIndx = mesh.lastIndx(s, k, direction);
                int* prevIndx = mesh.prevIndx(lastIndx, direction);
                int* twoPrevIndx = mesh.prevIndx(prevIndx, direction);
                F[lastIndx[0]][lastIndx[1]][lastIndx[2]] = secondOrder && N > 2 ? (4.0*arg[prevIndx] - arg[twoPrevIndx])/3.0 : arg[prevIndx];
            }
        }
    }
    delete [] otherDirections;
}

void problem::printF(argument* arg)
{
    for(int i=0;i<=mesh.getN(3);i++){
        std::cout << "razrez" << i << "\n";
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(1);k++){
                int* indx = mesh.indx(k, s, i);
                std::cout << arg[0][indx] << " ";
            }
            std::cout << "\n\n";
        }
    }
}

problem::~problem()
{
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            delete [] v1[i][s];
            delete [] v2[i][s];
            delete [] v3[i][s];
            delete [] p[i][s];
        }
    }
    for(int i=0;i<=mesh.getN(1);i++){
        delete [] v1[i];
        delete [] v2[i];
        delete [] v3[i];
        delete [] p[i];
    }
    delete [] v1;
    delete [] v2;
    delete [] v3;
    delete [] p;
    delete [] f;
}

int problem::signKroneker(int direction, int direction2)
{
    return direction == direction2 ? 1 : 0;
}

double problem::max(argument* arg)
{
    double max=0;
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                int* indx = mesh.indx(i, s, k);
                if(fabs(arg[0][indx])>max)
                    max = fabs(arg[0][indx]);
            }
        }
    }
    return max;
}

void problem::exportAllfunctions()
{
    arrargument V1(v1), V2(v2), V3(v3), P(p);
    result resv1(this, "v1.nb", std::ios::out);
    result resv2(this, "v2.nb", std::ios::out);
    result resv3(this, "v3.nb", std::ios::out);
    result resp(this, "p.nb", std::ios::out);
    resv1.writeDownFunction(&V1);
    resv2.writeDownFunction(&V2);
    resv3.writeDownFunction(&V3);
    resp.writeDownFunction(&P);
}

double*** problem::insertValues(double ***F, argument *arg)
{
    if (!F) {
        F = new double**[mesh.getN(1)+1];
        for(int i=0;i<=mesh.getN(1);i++)
            F[i] = new double*[mesh.getN(2)+1];
        for(int i=0;i<=mesh.getN(1);i++)
            for(int s=0;s<=mesh.getN(2);s++)
                F[i][s] = new double[mesh.getN(3)+1];
    }
    for(int i=0;i<=mesh.getN(1);i++)
        for(int s=0;s<=mesh.getN(2);s++)
            for(int k=0;k<=mesh.getN(3);k++){
                int* indx = mesh.indx(i, s, k);
                F[i][s][k]=arg[0][indx];
            }
    return F;
}
