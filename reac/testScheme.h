//
//  testScheme.h
//  reac
//
//  Created by Anton Kudryashov on 24.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__testScheme__
#define __reac__testScheme__

#include <stdio.h>
#include "scheme.h"

class testScheme : public scheme {
public:
    testScheme(){};
    void test();
    double puasson2(double ***ksi_0, double ***ksi_p_tmp);
    ~testScheme(){};
};

#endif /* defined(__reac__testScheme__) */
