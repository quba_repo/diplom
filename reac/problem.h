//
//  problem.h
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__problem__
#define __reac__problem__

#include <stdio.h>
#include "mesh.h"
#include "argument.h"
#include "result.h"

class problem {
    friend class result;
protected:
    mesh mesh;
    double*** v1;
    double*** v2;
    double*** v3;
    double*** p;
    double**** f;
    double tau;
    double tau2;
    double alpha;
public:
    problem();
    void updatePressureBounds(double*** P);
    void updateBounds(double*** F, int direction, bool bounds[2]);
    void updateBounds(double*** F, int direction, bool bounds[2], bool secondOrder);
    void printF(argument* F);
    int signKroneker(int direction, int direction2);
    double max(argument* arg);
    void exportAllfunctions();
    double*** insertValues(double*** F, argument* arg);
    ~problem();
};

#endif /* defined(__reac__problem__) */
