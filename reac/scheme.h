//
//  scheme.h
//  reac
//
//  Created by Anton Kudryashov on 04.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__scheme__
#define __reac__scheme__

#include <stdio.h>
#include "approximation.h"

class scheme : public approximation {
protected:
    double*** ksi_p;
    double*** ksi_1;
    double*** ksi_2;
    double*** ksi_3;
    double**** ksi;
    void stepZero();
    void stepOne();
    void stepTwo();
    void stepThree();
    void stepFour();
    void stepFive();
    void stepSixFinal();
    void tranferValues();
    void tranferValues(double*** F);
    double dh(int indx[3], int direction);
    double puasson(double*** ksi_0, double*** ksi_p_tmp);
public:
    scheme();
    void factorization();
    ~scheme();
};

#endif /* defined(__reac__scheme__) */
