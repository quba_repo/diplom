//
//  approximation.h
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__approximation__
#define __reac__approximation__

#include "problem.h"
#include <iostream>
#include "argument.h"

class approximation : public problem {
    double smoothValue(double value);
public:
    approximation();
    double derivative(argument* arg,
                      int indx[3],
                      int direction);
    double derivative(argument* arg,
                      int indx[3],
                      int direction,
                      int withSmooth);
    double averaged(argument* arg,
                    int indx1[3],
                    int indx2[3]);
    double derivativeSecond(argument* arg,
                            argument* middleArg,
                            int indx[3],
                            int direction);
    double derivativeSecond(argument* arg,
                            argument* middleArg,
                            int indx[3],
                            int outerDirection,
                            int innerDirection);
    void thomasAlgorithm(double*** F,
                         double* a,
                         double* b,
                         double* c,
                         double* d,
                         int direction,
                         bool* bounds,
                         int s,
                         int k);
    void cyclicThomasAlgorithm(double*** F,
                               double* a,
                               double* b,
                               double* c,
                               double* d,
                               int i,
                               int s);
    ~approximation(){};
};

#include <stdio.h>

#endif /* defined(__reac__approximation__) */
