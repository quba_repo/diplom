//
//  testScheme.cpp
//  reac
//
//  Created by Anton Kudryashov on 24.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "testScheme.h"
#include "constants.h"
#include "arrargument.h"
#include "funcargument.h"
#include "multiargument.h"
#include "distargument.h"
#include "result.h"
#include <math.h>

double f_mu(double x1, double x2, double x3);
double f_z11(double x1, double x2, double x3);
double f_z22(double x1, double x2, double x3);
double f_z33(double x1, double x2, double x3);
double f_H(double x1, double x2, double x3);
double ff(double x1, double x2, double x3)
{
    return 0;
}
double kT(double x1, double x2, double x3)
{
    return x2;
}

void testScheme::test()
{
    double*** ksi_p_tmp = new double**[mesh.getN(1)+1];
    double*** ksi_0 = new double**[mesh.getN(1)+1];
    for(int i=0;i<=mesh.getN(1);i++){
        ksi_p_tmp[i] = new double*[mesh.getN(2)+1];
        ksi_0[i] = new double*[mesh.getN(2)+1];
    }
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            ksi_p_tmp[i][s] = new double[mesh.getN(3)+1];
            ksi_0[i][s] = new double[mesh.getN(3)+1];
        }
    }
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                ksi_p_tmp[i][s][k] = 0;
                ksi_0[i][s][k] = 0;
            }
        }
    }
    int firstIndex = 1- mesh.getJ();
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            for(int k=firstIndex;k<=mesh.getN(3)-firstIndex;k++){
                ksi_0[i][s][k] = 0;
            }
        }
    }
    double dist = 1;
    int iterationsCount = _iterations2;
    double const epsilon = _epsilon2;
    int iter = 0;
    arrargument ksi0(ksi_0);
    funcargument ksiT(&kT, &mesh);
    distargument distT(&ksi0, &ksiT);
    while (dist > epsilon && iterationsCount > 0) {
        dist = puasson2(ksi_0, ksi_p_tmp);
        std::cout << "Пуассон\t" << dist << "\n";
        iterationsCount--;
        iter++;
        std::cout << "DIST=" << max(&distT);
    }
    result resk(this, "ksi0.nb", std::ios::out);
    resk.writeDownFunction(&ksi0);
    result reskt(this, "ksi0t.nb", std::ios::out);
    reskt.writeDownFunction(&ksiT);
}

double testScheme::puasson2(double ***ksi_0, double ***ksi_p_tmp)
{
    arrargument ksiP(ksi_p), ksi1(ksi_1), ksi2(ksi_2), ksi3(ksi_3), ksi0(ksi_0);
    funcargument z11(f_z11, &mesh), z22(f_z22, &mesh), z33(f_z33, &mesh);
    funcargument H(&f_H, &mesh), FF(&ff, &mesh);
    multiargument Hksi1(&H, &ksi1), Hz11(&H, &z11);
    int thirdIndex = 1 - mesh.getJ();
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            for(int k=thirdIndex;k<=mesh.getN(3)-thirdIndex;k++){
                ksi_p_tmp[i][s][k] = 0;
            }
        }
    }
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
                int* indx = mesh.indx(i, s, k);
                double secondDerivativies = (z11[indx]*derivativeSecond(&ksi0, &Hz11, indx, 1)/H[indx]
                                             + z22[indx]*derivativeSecond(&ksi0, &z22, indx, 2)
                                             + z33[indx]*derivativeSecond(&ksi0, &z33, indx, 3)/pow(H[indx], 2)
                                             );
                ksi_p_tmp[i][s][k] = secondDerivativies - FF[indx];
            }
        }
    }
    updatePressureBounds(ksi_p_tmp);
    bool* bounds1 = mesh.getPbounds(1);
    bool* bounds2 = mesh.getPbounds(2);
    bool* bounds3 = mesh.getPbounds(3);
    int direction = 1;
    for(int s=1;s<mesh.getN(2);s++){
        for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
            int N = mesh.getN(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for(int i=1;i<N;i++){
                int* indx = mesh.indx(i, s, k);
                int* nextIndx = mesh.nextIndx(indx, direction);
                int* prevIndx = mesh.prevIndx(indx, direction);
                double prevAvrg = averaged(&Hz11, indx, prevIndx);
                double nextAvrg = averaged(&Hz11, indx, nextIndx);
                double ratioTop = tau2*alpha*z11[indx];
                double ratioBot = H[indx]*pow(mesh.getH(direction), 2);
                a[i] = ratioTop*prevAvrg/ratioBot;
                b[i] = ratioTop*nextAvrg/ratioBot;
                c[i] = 1 + ratioTop*(prevAvrg+nextAvrg)/ratioBot;
                d[i] = ksi_p_tmp[i][s][k];
            }
            thomasAlgorithm(ksi_p_tmp, a, b, c, d, direction, bounds1, s, k);
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
    updateBounds(ksi_p_tmp, 2, bounds2);
    updateBounds(ksi_p_tmp, 3, bounds3);
    
    direction++;
    for(int i=1;i<mesh.getN(1);i++){
        for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
            int N = mesh.getN(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for(int s=1;s<N;s++){
                int* indx = mesh.indx(i, s, k);
                int* nextIndx = mesh.nextIndx(indx, direction);
                int* prevIndx = mesh.prevIndx(indx, direction);
                double prevAvrg = averaged(&z22, indx, prevIndx);
                double nextAvrg = averaged(&z22, indx, nextIndx);
                double ratioTop = tau2*alpha*z22[indx];
                double ratioBot = pow(mesh.getH(direction), 2);
                a[s] = ratioTop*prevAvrg/ratioBot;
                b[s] = ratioTop*nextAvrg/ratioBot;
                c[s] = 1 + ratioTop*(prevAvrg+nextAvrg)/ratioBot;
                d[s] = ksi_p_tmp[i][s][k];
            }
            thomasAlgorithm(ksi_p_tmp, a, b, c, d, direction, bounds2, i, k);
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
    updateBounds(ksi_p_tmp, 1, bounds1);
    updateBounds(ksi_p_tmp, 3, bounds3);
    direction++;
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            int N = mesh.getN(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for(int k=thirdIndex;k<=N-thirdIndex;k++){
                int* indx = mesh.indx(i, s, k);
                int* nextIndx = mesh.nextIndx(indx, direction);
                int* prevIndx = mesh.prevIndx(indx, direction);
                double prevAvrg = averaged(&z33, indx, prevIndx);
                double nextAvrg = averaged(&z33, indx, nextIndx);
                double ratioTop = tau2*alpha*z33[indx];
                double ratioBot = pow(mesh.getH(direction)*H[indx], 2);
                a[k] = ratioTop*prevAvrg/ratioBot;
                b[k] = ratioTop*nextAvrg/ratioBot;
                c[k] = 1 + ratioTop*(prevAvrg+nextAvrg)/ratioBot;
                d[k] = ksi_p_tmp[i][s][k];
            }
            if (mesh.getJ() == 0)
                thomasAlgorithm(ksi_p_tmp, a, b, c, d, direction, bounds3, i, s);
            else
                cyclicThomasAlgorithm(ksi_p_tmp, a, b, c, d, i, s);
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
    updateBounds(ksi_p_tmp, 1, bounds1);
    updateBounds(ksi_p_tmp, 2, bounds2);
    delete [] bounds1;
    delete [] bounds2;
    delete [] bounds3;
    double max=0;
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                ksi_0[i][s][k] += tau2*ksi_p_tmp[i][s][k];
                if(fabs(ksi_p_tmp[i][s][k])>max)
                    max = fabs(ksi_p_tmp[i][s][k]);
            }
        }
    }
    return max;
}
