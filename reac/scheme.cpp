//
//  scheme.cpp
//  reac
//
//  Created by Anton Kudryashov on 04.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "scheme.h"
#include "funcargument.h"
#include "arrargument.h"
#include "distargument.h"
#include "multiargument.h"
#include "constants.h"
#include <math.h>
#include "result.h"

double f_mu(double x1, double x2, double x3);
double f_z11(double x1, double x2, double x3);
double f_z22(double x1, double x2, double x3);
double f_z33(double x1, double x2, double x3);
double f_H(double x1, double x2, double x3);
double f_p(double x1, double x2, double x3);

int const kDerPuassonType = 1;

scheme::scheme()
{
    ksi_p = new double**[mesh.getN(1)+1];
    ksi_1 = new double**[mesh.getN(1)+1];
    ksi_2 = new double**[mesh.getN(1)+1];
    ksi_3 = new double**[mesh.getN(1)+1];
    for(int i=0;i<=mesh.getN(1);i++){
        ksi_p[i] = new double*[mesh.getN(2)+1];
        ksi_1[i] = new double*[mesh.getN(2)+1];
        ksi_2[i] = new double*[mesh.getN(2)+1];
        ksi_3[i] = new double*[mesh.getN(2)+1];
    }
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            ksi_p[i][s] = new double[mesh.getN(3)+1];
            ksi_1[i][s] = new double[mesh.getN(3)+1];
            ksi_2[i][s] = new double[mesh.getN(3)+1];
            ksi_3[i][s] = new double[mesh.getN(3)+1];
        }
    }
    ksi = new double***[4];
    ksi[0] = ksi_p;
    ksi[1] = ksi_1;
    ksi[2] = ksi_2;
    ksi[3] = ksi_3;
}

scheme::~scheme()
{
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            delete []ksi_p[i][s];
            delete []ksi_1[i][s];
            delete []ksi_2[i][s];
            delete []ksi_3[i][s];
        }
    }
    for(int i=0;i<=mesh.getN(1);i++){
        delete []ksi_p[i];
        delete []ksi_1[i];
        delete []ksi_2[i];
        delete []ksi_3[i];
    }
    delete []ksi_p;
    delete []ksi_1;
    delete []ksi_2;
    delete []ksi_3;
    delete []ksi;
}

void scheme::factorization()
{
    int iterations = _iterations1;
    double const epsilon = _epsilon1;
    double dist = 1;
    result res(this, "out.txt", std::ios::out);
    int K = 0;
    double*** prevKsi = NULL;
    const int range = 50;
    while (iterations > 0 && dist > epsilon) {
        arrargument ksiP(ksi_p), ksi1(ksi_1), ksi2(ksi_2), ksi3(ksi_3);
        arrargument P(p);
        stepZero();
        stepOne();
        stepTwo();
        stepThree();
        stepFour();
        stepFive();
        stepSixFinal();
        K = _iterations1 - iterations + 1;
        dist = max(&ksiP);
        if (K%range==0) {
            if (prevKsi) {
                arrargument prevKsiArg(prevKsi);
                distargument d(&prevKsiArg, &P);
                dist = max(&d)/(range*tau);
            }
            prevKsi = insertValues(prevKsi, &P);
            std::cout << "DIST " << dist << "\n";
        }
        std::cout << max(&ksiP) << "\t" << max(&ksi1) << "\t" << max(&ksi2) << "\t" << max(&ksi3) << "\t\t"<< K << "\n";
        res.compareSolution(K);
        iterations--;
    }
    delete [] prevKsi;
    result allResults(this, "outtable.txt", std::ios::app);
    allResults.compareSolutionInFinish(K);
    exportAllfunctions();
}

void scheme::stepZero()
{
    arrargument V1(v1), V2(v2), V3(v3), P(p);
    funcargument H(f_H, &mesh), mu(f_mu, &mesh);
    funcargument z11(f_z11, &mesh), z22(f_z22, &mesh), z33(f_z33, &mesh);
    multiargument V1V2(&V1, &V2), V1V3(&V1, &V3), V2V3(&V3, &V2);
    multiargument V1V1(&V1, &V1), V2V2(&V2, &V2), V3V3(&V3, &V3);
    multiargument muZ11(&mu, &z11), muZ22(&mu, &z22), muZ33(&mu, &z33), Hmu(&H, &mu);
    multiargument V1H(&H, &V1), V2H(&H, &V2), V3H(&H, &V3), muV1(&mu, &V1);
    multiargument V1V1H(&V1H, &V1), V1V2H(&V1V2, &H), HH(&H, &H);
    multiargument HmuZ11(&H, &muZ11), V1V3HH(&HH, &V1V3), HHmuZ11(&HH, &muZ11), V3muH(&V3H, &mu);
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                ksi_p[i][s][k] = 0;
                ksi_1[i][s][k] = 0;
                ksi_2[i][s][k] = 0;
                ksi_3[i][s][k] = 0;
            }
        }
    }
    int thirdIndex = 1 - mesh.getJ();
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
                int* indx = mesh.indx(i, s, k);
                ksi_p[i][s][k] = -( (z11[indx]*derivative(&V1H, indx, 1, kDerPuassonType)
                                     + z33[indx]*derivative(&V3, indx, 3, kDerPuassonType)
                                     )/H[indx]
                                   + z22[indx]*derivative(&V2, indx, 2, kDerPuassonType)
                                   );
                
                ksi_1[i][s][k] = -( z11[indx]*(derivative(&V1V1H, indx, 1)
                                                - 2*derivativeSecond(&V1, &HmuZ11, indx, 1)
                                                )/H[indx]
                                   + z11[indx]*derivative(&P, indx, 1, -kDerPuassonType)
                                   - mesh.getJ()*(V3V3[indx]
                                                  - 2*muV1[indx]/H[indx]
                                                  )/H[indx]
                                   + z22[indx]*(derivative(&V1V2, indx, 2)
                                                  - derivativeSecond(&V2, &muZ11, indx, 2, 1)
                                                  - derivativeSecond(&V1, &muZ22, indx, 2)
                                                  )
                                   + z33[indx]*(derivative(&V1V3, indx, 3)
                                                  - derivativeSecond(&V3, &muZ11, indx, 3, 1)
                                                  - derivativeSecond(&V1, &muZ33, indx, 3)/H[indx]
                                                  + mesh.getJ()*(3*mu[indx]*derivative(&V3, indx, 3)
                                                                 + V3[indx]*derivative(&mu, indx, 3)
                                                                 )/H[indx]
                                                  )/H[indx]
                                   );
                ksi_2[i][s][k] = -( z11[indx]*(derivative(&V1V2H, indx, 1)
                                                 - derivativeSecond(&V2, &HmuZ11, indx, 1)
                                                 - z22[indx]*derivativeSecond(&V1, &Hmu, indx, 1, 2)
                                                 )/H[indx]
                                   + z22[indx]*(derivative(&V2V2, indx, 2)
                                                  + derivative(&P, indx, 2, -kDerPuassonType)
                                                  - 2*derivativeSecond(&V2, &muZ22, indx, 2)
                                                  )
                                   + z33[indx]*(derivative(&V2V3, indx, 3)
                                                  - z22[indx]*derivativeSecond(&V3, &mu, indx, 3, 2)
                                                  - derivativeSecond(&V2, &muZ33, indx, 3)/H[indx]
                                                  )/H[indx]
                                   );
                ksi_3[i][s][k] = -( z11[indx]*(derivative(&V1V3HH, indx, 1)
                                                 - derivativeSecond(&V3, &HHmuZ11, indx, 1)
                                                 - z33[indx]*derivativeSecond(&V1, &Hmu, indx, 1, 3)
                                                 )/(H[indx]*H[indx])
                                   + z22[indx]*(derivative(&V2V3, indx, 2)
                                                  - derivativeSecond(&V3, &muZ22, indx, 2)
                                                  - z33[indx]*(derivativeSecond(&V2, &mu, indx, 2, 3))/H[indx]
                                                  )
                                   + z33[indx]*(derivative(&V3V3, indx, 3)
                                                  + derivative(&P, indx, 3, -kDerPuassonType)
                                                  - 2*derivativeSecond(&V3, &muZ33, indx, 3)/H[indx]
                                                  )/H[indx]
                                   + mesh.getJ()*(z11[indx]*derivative(&V3muH, indx, 1)
                                                  - 2*z33[indx]*derivative(&muV1, indx, 3)
                                                  )/(H[indx]*H[indx])
                                   );
            }
        }
    }
    updatePressureBounds(ksi_p);
    //tranferValues();
}

void scheme::stepOne()
{
    double*** ksi_p_tmp = new double**[mesh.getN(1)+1];
    double*** ksi_0 = new double**[mesh.getN(1)+1];
    for(int i=0;i<=mesh.getN(1);i++){
        ksi_p_tmp[i] = new double*[mesh.getN(2)+1];
        ksi_0[i] = new double*[mesh.getN(2)+1];
    }
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            ksi_p_tmp[i][s] = new double[mesh.getN(3)+1];
            ksi_0[i][s] = new double[mesh.getN(3)+1];
        }
    }
    /*
    tau2 = 1;
    result res(this, "opt_tau2.txt", std::ios::out);
    for (int l=1; tau2 > 0.001; l++) {
    //*/
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                ksi_p_tmp[i][s][k] = 0;
                ksi_0[i][s][k] = 0;
            }
        }
    }
    int firstIndex = 1- mesh.getJ();
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            for(int k=firstIndex;k<=mesh.getN(3)-firstIndex;k++){
                ksi_0[i][s][k] = 0;
            }
        }
    }
    arrargument ksi_ptmp(ksi_p_tmp);
    double dist = 1;
    int iterationsCount = _iterations2;
    double const epsilon = _epsilon2;
    int iter = 0;
    while (dist > epsilon && iterationsCount > 0) {
        dist = puasson(ksi_0, ksi_p_tmp);
        std::cout << "Пуассон\t" << dist << "\n";
        iterationsCount--;
        iter++;
    }
        /*
        res.writeValues(iter, tau2);
        tau2 -= 1.0/100.0;
    }
    throw std::invalid_argument("Значения получены");
         //*/
    updatePressureBounds(ksi_0);
    for(int i=0;i<=mesh.getN(1);i++)
        for(int s=0;s<=mesh.getN(2);s++)
            for(int k=0;k<=mesh.getN(3);k++)
                ksi_p[i][s][k]=ksi_0[i][s][k];
    funcargument z11(f_z11, &mesh), z22(f_z22, &mesh), z33(f_z33, &mesh), H(f_H, &mesh);
    arrargument ksiP(ksi_p);
    int thirdIndex = 1 - mesh.getJ();
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
                int* indx = mesh.indx(i, s, k);
                ksi_1[i][s][k] -= tau*alpha*z11[indx]*derivative(&ksiP, indx, 1, -kDerPuassonType);
                ksi_2[i][s][k] -= tau*alpha*z22[indx]*derivative(&ksiP, indx, 2, -kDerPuassonType);
                ksi_3[i][s][k] -= tau*alpha*z33[indx]*derivative(&ksiP, indx, 3, -kDerPuassonType)/H[indx];
            }
        }
    }
    //tranferValues();
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            delete []ksi_p_tmp[i][s];
            delete []ksi_0[i][s];
        }
    }
    for(int i=0;i<=mesh.getN(1);i++){
        delete []ksi_p_tmp[i];
        delete []ksi_0[i];
    }
    delete []ksi_0;
    delete []ksi_p_tmp;
}
double scheme::puasson(double ***ksi_0, double ***ksi_p_tmp)
{
    arrargument ksiP(ksi_p), ksi1(ksi_1), ksi2(ksi_2), ksi3(ksi_3), ksi0(ksi_0), ksiPtmp(ksi_p_tmp);
    funcargument z11(f_z11, &mesh), z22(f_z22, &mesh), z33(f_z33, &mesh);
    funcargument H(&f_H, &mesh);
    multiargument Hksi1(&H, &ksi1), Hz11(&H, &z11);
    int thirdIndex = 1 - mesh.getJ();
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
                int* indx = mesh.indx(i, s, k);
                double secondDerivativies = (z11[indx]*derivativeSecond(&ksi0, &Hz11, indx, 1)/H[indx]
                                             + z22[indx]*derivativeSecond(&ksi0, &z22, indx, 2)
                                             + z33[indx]*derivativeSecond(&ksi0, &z33, indx, 3)/pow(H[indx], 2)
                                             );
                double firstDerivativies = (z11[indx]*derivative(&Hksi1, indx, 1, kDerPuassonType)/H[indx]
                                          + z22[indx]*derivative(&ksi2, indx, 2, kDerPuassonType)
                                          + z33[indx]*derivative(&ksi3, indx, 3, kDerPuassonType)/H[indx]
                                          )/(alpha*tau);
                ksi_p_tmp[i][s][k] = secondDerivativies - firstDerivativies + ksi_p[i][s][k]/(pow(tau*alpha,2));
            }
        }
    }
    updatePressureBounds(ksi_p_tmp);
    bool* bounds1 = mesh.getPbounds(1);
    bool* bounds2 = mesh.getPbounds(2);
    bool* bounds3 = mesh.getPbounds(3);
    int direction = 1;
    for(int s=1;s<mesh.getN(2);s++){
        for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
            int N = mesh.getN(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for(int i=1;i<N;i++){
                int* indx = mesh.indx(i, s, k);
                int* nextIndx = mesh.nextIndx(indx, direction);
                int* prevIndx = mesh.prevIndx(indx, direction);
                double prevAvrg = averaged(&Hz11, indx, prevIndx);
                double nextAvrg = averaged(&Hz11, indx, nextIndx);
                double ratioTop = tau2*alpha*z11[indx];
                double ratioBot = H[indx]*pow(mesh.getH(direction), 2);
                a[i] = ratioTop*prevAvrg/ratioBot;
                b[i] = ratioTop*nextAvrg/ratioBot;
                c[i] = 1 + ratioTop*(prevAvrg+nextAvrg)/ratioBot;
                d[i] = ksi_p_tmp[i][s][k];
            }
            thomasAlgorithm(ksi_p_tmp, a, b, c, d, direction, bounds1, s, k);
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
    updatePressureBounds(ksi_p_tmp);
    direction++;
    for(int i=1;i<mesh.getN(1);i++){
        for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
            int N = mesh.getN(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for(int s=1;s<N;s++){
                int* indx = mesh.indx(i, s, k);
                int* nextIndx = mesh.nextIndx(indx, direction);
                int* prevIndx = mesh.prevIndx(indx, direction);
                double prevAvrg = averaged(&z22, indx, prevIndx);
                double nextAvrg = averaged(&z22, indx, nextIndx);
                double ratioTop = tau2*alpha*z22[indx];
                double ratioBot = pow(mesh.getH(direction), 2);
                a[s] = ratioTop*prevAvrg/ratioBot;
                b[s] = ratioTop*nextAvrg/ratioBot;
                c[s] = 1 + ratioTop*(prevAvrg+nextAvrg)/ratioBot;
                d[s] = ksi_p_tmp[i][s][k];
            }
            thomasAlgorithm(ksi_p_tmp, a, b, c, d, direction, bounds2, i, k);
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
    updatePressureBounds(ksi_p_tmp);
    direction++;
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            int N = mesh.getN(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for(int k=thirdIndex;k<=N-thirdIndex;k++){
                int* indx = mesh.indx(i, s, k);
                int* nextIndx = mesh.nextIndx(indx, direction);
                int* prevIndx = mesh.prevIndx(indx, direction);
                double prevAvrg = averaged(&z33, indx, prevIndx);
                double nextAvrg = averaged(&z33, indx, nextIndx);
                double ratioTop = tau2*alpha*z33[indx];
                double ratioBot = pow(mesh.getH(direction)*H[indx], 2);
                a[k] = ratioTop*prevAvrg/ratioBot;
                b[k] = ratioTop*nextAvrg/ratioBot;
                c[k] = 1 + ratioTop*(prevAvrg+nextAvrg)/ratioBot;
                d[k] = ksi_p_tmp[i][s][k];
            }
            if (mesh.getJ() == 0)
                thomasAlgorithm(ksi_p_tmp, a, b, c, d, direction, bounds3, i, s);
            else
                cyclicThomasAlgorithm(ksi_p_tmp, a, b, c, d, i, s);
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
    updatePressureBounds(ksi_p_tmp);
    delete [] bounds1;
    delete [] bounds2;
    delete [] bounds3;
    double max=0;
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                ksi_0[i][s][k] += tau2*ksi_p_tmp[i][s][k];
                if(fabs(ksi_p_tmp[i][s][k])>max)
                    max = fabs(ksi_p_tmp[i][s][k]);
            }
        }
    }
    return max;
}

void scheme::stepTwo()
{
    funcargument z11(f_z11, &mesh);
    funcargument H(&f_H, &mesh), mu(&f_mu, &mesh);
    multiargument Hz11(&H, &z11);
    multiargument Hz11mu(&Hz11, &mu);
    int thirdIndex = 1 - mesh.getJ();
    for(int s=1;s<mesh.getN(2);s++){
        for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
            int const direction = 1;
            int N = mesh.getN(direction);
            double h = mesh.getH(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for (int direction2 = 1; direction2 <=3; direction2++) {
                for(int i=1;i<N;i++){
                    int* indx = mesh.indx(i, s, k);
                    int* nextIndx = mesh.nextIndx(indx, direction);
                    int* prevIndx = mesh.prevIndx(indx, direction);
                    double prevAvrg = averaged(&Hz11mu, indx, prevIndx);
                    double nextAvrg = averaged(&Hz11mu, indx, nextIndx);
                    double ratioTop = tau*alpha*z11[indx];
                    double ratioBot = pow(h, 2)*H[indx];
                    double ratioTop2 = v1[i][s][k] + signKroneker(direction2, 3)*mesh.getJ()*mu[indx]/H[indx];
                    a[i] = ratioTop*ratioTop2/(2*h) + (1 + signKroneker(direction, direction2))*ratioTop*prevAvrg/ratioBot;
                    b[i] = -ratioTop*ratioTop2/(2*h) + (1 + signKroneker(direction, direction2))*ratioTop*nextAvrg/ratioBot;
                    c[i] = 1 + ((1 + signKroneker(direction, direction2))*ratioTop*(prevAvrg+nextAvrg))/ratioBot
                    + dh(indx, direction2);
                    d[i] = ksi[direction2][i][s][k];
                }
                bool* bounds = mesh.getVbounds(direction);
                thomasAlgorithm(ksi[direction2], a, b, c, d, direction, bounds, s, k);
                delete [] bounds;
            }
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
    //tranferValues();
}
double scheme::dh(int indx[3], int direction)
{
    funcargument H(&f_H, &mesh), mu(&f_mu, &mesh);
    funcargument z11(f_z11, &mesh);
    multiargument Hmu(&H, &mu);
    switch (direction) {
        case 1:
            return mesh.getJ()*tau*alpha*2*mu[indx]/(H[indx]*H[indx]);
        case 2:
            return 0;
        case 3:
            return mesh.getJ()*tau*alpha*z11[indx]*derivative(&Hmu, indx, direction)/(H[indx]*H[indx]);
        default:
            return mesh.directionIsCorrect(direction);
    }
}

void scheme::stepThree()
{
    funcargument z22(f_z22, &mesh);
    funcargument H(&f_H, &mesh), mu(&f_mu, &mesh);
    multiargument z22mu(&mu, &z22);
    int thirdIndex = 1 - mesh.getJ();
    for(int i=1;i<mesh.getN(1);i++){
        for(int k=thirdIndex;k<=mesh.getN(3) - thirdIndex;k++){
            int const direction = 2;
            int N = mesh.getN(direction);
            double h = mesh.getH(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for (int direction2 = 1; direction2 <=3; direction2++) {
                for(int s=1;s<N;s++){
                    int* indx = mesh.indx(i, s, k);
                    int* nextIndx = mesh.nextIndx(indx, direction);
                    int* prevIndx = mesh.prevIndx(indx, direction);
                    double prevAvrg = averaged(&z22mu, indx, prevIndx);
                    double nextAvrg = averaged(&z22mu, indx, nextIndx);
                    double ratioTop = tau*alpha*z22[indx];
                    double ratioBot = pow(h, 2);
                    a[s] = ratioTop*v2[i][s][k]/(2*h) + (1 + signKroneker(direction, direction2))*ratioTop*prevAvrg/ratioBot;
                    b[s] = -ratioTop*v2[i][s][k]/(2*h) + (1 + signKroneker(direction, direction2))*ratioTop*nextAvrg/ratioBot;
                    c[s] = 1 + ((1 + signKroneker(direction, direction2))*ratioTop*(prevAvrg+nextAvrg))/ratioBot;
                    d[s] = ksi[direction2][i][s][k];
                }
                bool* bounds = mesh.getVbounds(direction);
                thomasAlgorithm(ksi[direction2], a, b, c, d, direction, bounds, i, k);
                delete [] bounds;
            }
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
    //tranferValues();
}

void scheme::stepFour()
{
    funcargument z33(f_z33, &mesh);
    funcargument H(&f_H, &mesh), mu(&f_mu, &mesh);
    multiargument z33mu(&mu, &z33);
    for(int i=1;i<mesh.getN(1);i++){
        for(int s=1;s<mesh.getN(2);s++){
            int thirdIndex = 1 - mesh.getJ();
            int const direction = 3;
            int N = mesh.getN(direction);
            double h = mesh.getH(direction);
            double* a = new double[N+1];
            double* b = new double[N+1];
            double* c = new double[N+1];
            double* d = new double[N+1];
            for (int direction2 = 1; direction2 <=3; direction2++) {
                for(int k=thirdIndex;k<=N-thirdIndex;k++){
                    int* indx = mesh.indx(i, s, k);
                    int* nextIndx = mesh.nextIndx(indx, direction);
                    int* prevIndx = mesh.prevIndx(indx, direction);
                    double prevAvrg = averaged(&z33mu, indx, prevIndx);
                    double nextAvrg = averaged(&z33mu, indx, nextIndx);
                    double ratioTop = tau*alpha*z33[indx];
                    double ratioBot = pow(h*H[indx], 2);
                    a[k] = ratioTop*v3[i][s][k]/(2*h*H[indx]) + (1 + signKroneker(direction, direction2))*ratioTop*prevAvrg/ratioBot;
                    b[k] = -ratioTop*v3[i][s][k]/(2*h*H[indx]) + (1 + signKroneker(direction, direction2))*ratioTop*nextAvrg/ratioBot;
                    c[k] = 1 + ((1 + signKroneker(direction, direction2))*ratioTop*(prevAvrg+nextAvrg))/ratioBot;
                    d[k] = ksi[direction2][i][s][k];
                }
                if (mesh.getJ() == 0){
                    bool* bounds = mesh.getVbounds(direction);
                    thomasAlgorithm(ksi[direction2], a, b, c, d, direction, bounds, i, s);
                    delete [] bounds;
                }
                else
                    cyclicThomasAlgorithm(ksi[direction2], a, b, c, d, i, s);
            }
            delete [] a;
            delete [] b;
            delete [] c;
            delete [] d;
        }
    }
   //tranferValues();
}

void scheme::stepFive()
{
    funcargument H(&f_H, &mesh);
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                //int* indx = mesh.indx(i, s, k);/*
                double omega = tau*alpha*v3[i][s][k];
                double ksi1 = (ksi_1[i][s][k] + omega*ksi_3[i][s][k])/(1+omega*omega);
                double ksi3 = (ksi_3[i][s][k] - omega*ksi_1[i][s][k])/(1+omega*omega);
                ksi_1[i][s][k] = ksi1;
                ksi_3[i][s][k] = ksi3;
                /*/
                ksi_3[i][s][k] = (ksi_3[i][s][k] - (mesh.getJ()*(tau*alpha)*v3[i][s][k]*ksi_1[i][s][k])/H[indx]
                                  )/(1 + mesh.getJ()*(tau*alpha)*(v1[i][s][k] + (2.0*tau*alpha*pow(v3[i][s][k],2))/H[indx])/H[indx]);
                ksi_1[i][s][k] += 2.0*mesh.getJ()*v3[i][s][k]*tau*alpha*ksi_3[i][s][k]/H[indx];
                 */
            }
        }
    }
}

void scheme::stepSixFinal()
{
    tranferValues();
    for(int i=0;i<=mesh.getN(1);i++){
        for(int s=0;s<=mesh.getN(2);s++){
            for(int k=0;k<=mesh.getN(3);k++){
                p[i][s][k] = p[i][s][k] + tau*ksi_p[i][s][k];
                v1[i][s][k] = v1[i][s][k] + tau*ksi_1[i][s][k];
                v2[i][s][k] = v2[i][s][k] + tau*ksi_2[i][s][k];
                v3[i][s][k] = v3[i][s][k] + tau*ksi_3[i][s][k];
            }
        }
    }
}

void scheme::tranferValues()
{
    for (int i=0; i<4; i++)
        tranferValues(ksi[i]);
}

void scheme::tranferValues(double*** F)
{
    if (!_withTransfer)
        return;
    int direction = _transferableDirection;
    bool* bounds = new bool[2];
    bounds[0] = true;
    bounds[1] = true;
    updateBounds(F, direction, bounds, false);
    delete [] bounds;
}