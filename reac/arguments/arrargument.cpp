//
//  arrargument.cpp
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "arrargument.h"

arrargument::arrargument(double*** Array)
{
    if (!!Array)
        array = Array;
}

double arrargument::operator[](int indx[3])
{
    return array[indx[0]][indx[1]][indx[2]];
}

arrargument::~arrargument()
{
    array = NULL;
}