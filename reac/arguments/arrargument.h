//
//  arrargument.h
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__arrargument__
#define __reac__arrargument__

#include <stdio.h>
#include "argument.h"

class arrargument : public argument {
    double*** array;
public:
    arrargument(double*** array);
    double operator[](int indx[3]);
    ~arrargument();
};

#endif /* defined(__reac__arrargument__) */
