//
//  funcargument.cpp
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "funcargument.h"

funcargument::funcargument(double Func(double x1, double x2, double x3), class mesh* Mesh)
{
    func = Func;
    mesh = Mesh;
}

double funcargument::operator[](int indx[3])
{
    double x1 = mesh->getA(1) + mesh->getH(1)*indx[0];
    double x2 = mesh->getA(2) + mesh->getH(2)*indx[1];
    double x3 = mesh->getA(3) + mesh->getH(3)*indx[2];
    return func(x1, x2, x3);
}

funcargument::~funcargument()
{
    func = NULL;
    mesh = NULL;
}