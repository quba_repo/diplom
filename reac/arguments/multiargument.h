//
//  multiargument.h
//  reac
//
//  Created by Anton Kudryashov on 04.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__multiargument__
#define __reac__multiargument__

#include <stdio.h>
#include "argument.h"

class multiargument : public argument {
    argument* arg1;
    argument* arg2;
public:
    multiargument(argument* Arg1, argument* Arg2);
    double operator[](int indx[3]);
    ~multiargument();
};

#endif /* defined(__reac__multiargument__) */
