//
//  funcargument.h
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__funcargument__
#define __reac__funcargument__

#include <stdio.h>
#include "argument.h"
#include "mesh.h"

class funcargument : public argument {
    double (*func)(double x1, double x2, double x3);
    mesh* mesh;
public:
    funcargument(double Func(double x1, double x2, double x3), class mesh* Mesh);
    double operator[](int indx[3]);
    ~funcargument();
};

#endif /* defined(__reac__funcargument__) */
