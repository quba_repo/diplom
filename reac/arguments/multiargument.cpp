//
//  multiargument.cpp
//  reac
//
//  Created by Anton Kudryashov on 04.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "multiargument.h"
#include <stdarg.h>

multiargument::multiargument(argument* Arg1, argument* Arg2)
{
    arg1 = Arg1;
    arg2 = Arg2;
}

double multiargument::operator[](int *indx)
{
    return arg1[0][indx]*arg2[0][indx];
}

multiargument::~multiargument()
{
    arg1 = NULL;
    arg2 = NULL;
}