//
//  argument.h
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__argument__
#define __reac__argument__

class argument {
    
public:
    argument(){};
    double virtual operator[](int indx[3]) = 0;
    ~argument(){};
};

#include <stdio.h>

#endif /* defined(__reac__argument__) */
