//
//  distargument.cpp
//  reac
//
//  Created by Anton Kudryashov on 11.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "distargument.h"

distargument::distargument(argument* Arg1, argument* Arg2)
{
    arg1 = Arg1;
    arg2 = Arg2;
}

double distargument::operator[](int *indx)
{
    return arg1[0][indx] - arg2[0][indx];
}

distargument::~distargument()
{
    arg1 = NULL;
    arg2 = NULL;
}