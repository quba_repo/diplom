//
//  distargument.h
//  reac
//
//  Created by Anton Kudryashov on 11.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#ifndef __reac__distargument__
#define __reac__distargument__

#include <stdio.h>
#include "argument.h"

class distargument : public argument {
    argument* arg1;
    argument* arg2;
public:
    distargument(argument* Arg1, argument* Arg2);
    double operator[](int indx[3]);
    ~distargument();
};

#endif /* defined(__reac__distargument__) */
