//
//  mesh.cpp
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "mesh.h"
#include "constants.h"
#include <stdexcept>

mesh::mesh()
{
    A1 = a1;
    A2 = a2;
    A3 = a3;
    B1 = b1;
    B2 = b2;
    B3 = b3;
    
    if (_h1<0) {
        N1 = _N1 - 1;
        h1 = (B1 - A1)/N1;
    } else {
        h1 = _h1;
        N1 = (B1 - A1)/h1;
    }
    
    if (_h2<0) {
        N2 = _N2 - 1;
        h2 = (B2 - A2)/N2;
    } else {
        h2 = _h2;
        N2 = (B2 - A2)/h2;
    }
    
    if (_h3<0) {
        N3 = _N3 - 1;
        h3 = (B3 - A3)/N3;
    } else {
        h3 = _h3;
        N3 = (B3 - A3)/h3;
    }
    j = _j;
    indexes = new int***[N1+1];
    for(int i=0;i<=N1;i++)
        indexes[i] = new int**[N2+1];
    for(int i=0;i<=N1;i++)
        for(int s=0;s<=N2;s++)
            indexes[i][s] = new int*[N3+1];
    for(int i=0;i<=N1;i++)
        for(int s=0;s<=N2;s++)
            for(int k=0;k<=N3;k++)
                indexes[i][s][k] = new int[3];
    for(int i=0;i<=N1;i++)
        for(int s=0;s<=N2;s++)
            for(int k=0;k<=N3;k++){
                indexes[i][s][k][0] = i;
                indexes[i][s][k][1] = s;
                indexes[i][s][k][2] = k;
            }
}
mesh::~mesh()
{
    for(int i=0;i<=N1;i++)
        for(int s=0;s<=N2;s++)
            for(int k=0;k<=N3;k++)
                delete indexes[i][s][k];
    for(int i=0;i<=N1;i++)
        for(int s=0;s<=N2;s++)
            delete[] indexes[i][s];
    for(int i=0;i<=N1;i++)
        delete[] indexes[i];
    delete[] indexes;
}

int* mesh::indx(int i, int s, int k)
{
    if (!indxIsCorrect(i, s, k))
        return NULL;
    return indexes[i][s][k];
}
int* mesh::indxByDirection(int ibyDirection, int k, int s, int direction)
{
    if (!directionIsCorrect(direction))
        return NULL;
    int ii = direction == 1 ? ibyDirection : k;
    int ss = direction == 2 ? ibyDirection : (direction == 1 ? k : s);
    int kk = direction == 3 ? ibyDirection : s;
    return indx(ii, ss, kk);
}
int* mesh::prevIndx(int *indx, int direction)
{
    if (!directionIsCorrect(direction))
        return NULL;
    int iiter = direction == 1 ? 1 : 0;
    int siter = direction == 2 ? 1 : 0;
    int kiter = direction == 3 ? 1 : 0;
    int lastIndex = j==1 && indx[2] == 0 ? N3 : indx[2];
    return this->indx(indx[0] - iiter, indx[1] - siter, lastIndex - kiter);
}
int* mesh::nextIndx(int *indx, int direction)
{
    if (!directionIsCorrect(direction))
        return NULL;
    int iiter = direction == 1 ? 1 : 0;
    int siter = direction == 2 ? 1 : 0;
    int kiter = direction == 3 ? 1 : 0;
    int lastIndex = j==1 && indx[2] == N3 ? 0 : indx[2];
    return this->indx(indx[0] + iiter, indx[1] + siter, lastIndex + kiter);
}
int* mesh::firstIndx(int s, int k, int direction)
{
    return indxByDirection(0, s, k, direction);
}
int* mesh::lastIndx(int s, int k, int direction)
{
    return indxByDirection(getN(direction), s, k, direction);
}

double mesh::getA(int direction)
{
    switch (direction) {
        case 1:
            return A1;
        case 2:
            return A2;
        case 3:
            return A3;
        default:
            return directionIsCorrect(direction);
    }
}

int mesh::getN(int direction)
{
    switch (direction) {
        case 1:
            return N1;
        case 2:
            return N2;
        case 3:
            return N3;
        default:
            return directionIsCorrect(direction);
    }
}

double mesh::getH(int direction)
{
    switch (direction) {
        case 1:
            return h1;
        case 2:
            return h2;
        case 3:
            return h3;
        default:
            return directionIsCorrect(direction);
    }
}

int mesh::getJ()
{
    return j;
}

bool* mesh::getPbounds(int direction)
{
    if (directionIsCorrect(direction)) {
        bool* bounds = new bool[2];
        switch (direction) {
            case 1:{
                bounds[0] = _p_left1;
                bounds[1] = _p_right1;
                break;
            }
            case 2:{
                bounds[0] = _p_left2;
                bounds[1] = _p_right2;
                break;
            }
            case 3:{
                bounds[0] = _p_left3;
                bounds[1] = _p_right3;
                break;
            }
            default:
                break;
        }
        return bounds;
    }
    return NULL;
}

bool* mesh::getVbounds(int direction)
{
    if (directionIsCorrect(direction)) {
        bool* bounds = new bool[2];
        switch (direction) {
            case 1:{
                bounds[0] = _v_left1;
                bounds[1] = _v_right1;
                break;
            }
            case 2:{
                bounds[0] = _v_left2;
                bounds[1] = _v_right2;
                break;
            }
            case 3:{
                bounds[0] = _v_left3;
                bounds[1] = _v_right3;
                break;
            }
            default:
                break;
        }
        return bounds;
    }
    return NULL;
}

bool mesh::directionIsCorrect(int direction)
{
    bool correct = direction >= 1 && direction <= 3;
    if (!correct)
        throw std::invalid_argument("Некорректно задана размерность");
    return correct;
}

bool mesh::indxIsCorrect(int i, int s, int k)
{
    bool lessThanN = i <= N1 && s <= N2 && k <= N3;
    bool moreThanZero = i >=0 && s>= 0 && k >= 0;
    bool correct = moreThanZero && lessThanN;
    if (!correct)
        throw std::invalid_argument("Некорректно задан индекс");
    return correct;
}

int* mesh::otherDirections(int direction)
{
    if (!directionIsCorrect(direction))
        return NULL;
    int* otherDirections = new int[2];
    otherDirections[0] = direction+1>3 ? (direction+1)%3 : direction+1;
    otherDirections[1] = direction+2>3 ? (direction+2)%3 : direction+2;
    if (otherDirections[1] < otherDirections[0]) {
        int tmpDir = otherDirections[0];
        otherDirections[0] = otherDirections[1];
        otherDirections[1] = tmpDir;
    }
    return otherDirections;
}



