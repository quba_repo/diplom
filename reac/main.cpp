//
//  main.cpp
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include <stdio.h>
#include "scheme.h"
#include <stdexcept>

int main(int argc, const char * argv[])
{
    try {
        scheme sch;
        sch.factorization();
    } catch (std::invalid_argument &e) {
        std::cout << "Выход с ошибкой:\t" << e.what() << "\n";
        return 1;
    }
    return 0;
}