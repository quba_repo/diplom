//
//  approximation.cpp
//  reac
//
//  Created by Anton Kudryashov on 03.10.14.
//  Copyright (c) 2014 Anton Kudryashov. All rights reserved.
//

#include "approximation.h"
#include "constants.h"
#include "arrargument.h"
#include <math.h>

const double kSmoothCoeficient = 1.0e-30;

approximation::approximation()
{
    //nothing
}

double approximation::smoothValue(double value)
{
    return value; //fabs(value) < kSmoothCoeficient ? 0 : value;
}

double approximation::averaged(argument *arg,
                        int *indx1,
                        int *indx2)
{
    if (!arg)
        return 1;
    double Arg1 = arg[0][indx1];
    double Arg2 = arg[0][indx2];
    return (Arg1 + Arg2)/2;
}

double approximation::derivative(argument* arg,
                                 int indx[3],
                                 int direction)
{
    if (!mesh.directionIsCorrect(direction))
        return 0;
    int* indxprev = mesh.prevIndx(indx, direction);
    int* indxnext = mesh.nextIndx(indx, direction);
    double prev = arg[0][indxprev];
    double next = arg[0][indxnext];
    double h = mesh.getH(direction);
    double value = (next - prev)/(2*h);
    return smoothValue(value);
}

double approximation::derivative(argument *arg,
                                 int *indx,
                                 int direction,
                                 int withSmooth)
{
    double symDerivative = derivative(arg, indx, direction);
    if (withSmooth==0)
        return symDerivative;
    arrargument func(f[direction]);
    int sign = func[indx] > 0 ? 1 : -1;
    int* nextIndx = mesh.nextIndx(indx, direction);
    int* prevIndx = mesh.prevIndx(indx, direction);
    double chisl = fabs(arg[0][nextIndx] - arg[0][indx]) + fabs(arg[0][prevIndx] - arg[0][indx]);
    double e = chisl < kSmoothCoeficient ? 0 : fabs(arg[0][prevIndx] - 2*arg[0][indx] + arg[0][nextIndx])/chisl;
    
    double value = symDerivative + withSmooth*sign*e*(arg[0][prevIndx] - 2*arg[0][indx] + arg[0][nextIndx])/(2*mesh.getH(direction));
    return smoothValue(value);
}

double approximation::derivativeSecond(argument *arg,
                                       argument *middleArg,
                                       int *indx,
                                       int direction)
{
    if (!mesh.directionIsCorrect(direction))
        return 0;
    int* indxprev = mesh.prevIndx(indx, direction);
    int* indxnext = mesh.nextIndx(indx, direction);
    double prevArg = arg[0][indxprev];
    double currArg = arg[0][indx];
    double nextArg = arg[0][indxnext];
    double prevMiddleArg = averaged(middleArg, indxprev, indx);
    double nextMiddleArg = averaged(middleArg, indxnext, indx);
    double h = mesh.getH(direction);
    double nextValue = nextMiddleArg*(nextArg - currArg);
    double prevValue = prevMiddleArg*(currArg - prevArg);
    double value = (nextValue - prevValue)/(h*h);
    return smoothValue(value);
}

double approximation::derivativeSecond(argument *arg,
                                       argument *middleArg,
                                       int *indx,
                                       int outerDirection,
                                       int innerDirection)
{
    if (!mesh.directionIsCorrect(innerDirection) ||
        !mesh.directionIsCorrect(outerDirection))
        return 0;
    if (innerDirection == outerDirection)
        return derivativeSecond(arg, middleArg, indx, innerDirection);
    int* indxprev = mesh.prevIndx(indx, outerDirection);
    int* indxnext = mesh.nextIndx(indx, outerDirection);
    double inNextDerivative = derivative(arg, indxnext, innerDirection);
    double inPrevDerivative = derivative(arg, indxprev, innerDirection);
    inNextDerivative *= !middleArg ? 1 : middleArg[0][indxnext];
    inPrevDerivative *= !middleArg ? 1 : middleArg[0][indxprev];
    double h = mesh.getH(outerDirection);
    double value = (inNextDerivative - inPrevDerivative)/(2*h);
    return value;
}

void approximation::thomasAlgorithm(double ***F,
                                    double *a,
                                    double *b,
                                    double *c,
                                    double *d,
                                    int direction,
                                    bool *bounds,
                                    int s,
                                    int k)
{
    if (!mesh.directionIsCorrect(direction))
        return;
    int N = mesh.getN(direction);
    arrargument argF(F);
    double* alp = new double[N+1];
    double* beta = new double[N+1];
    if (bounds) {
        alp[1] = bounds[0] ? 1 : 0;
        beta[1]= bounds[0] ? 0 : 0;
        //alp[1] = bounds[0] ? (4*b[1]- c[1])/(3*b[1] - a[1]) : 0;
        //beta[1]= bounds[0] ? d[1]/(3*b[1] - a[1]) : 0;
    } else {
        alp[1] = 0;
        beta[1] = 0;
    }
    for(int i=1;i<N;i++){
        alp[i+1] = b[i]/(c[i]-a[i]*alp[i]);
        beta[i+1]= (d[i] + a[i]*beta[i])/(c[i]-a[i]*alp[i]);
    }
    if (bounds){
        int* lastIndx = mesh.lastIndx(s, k, direction);
        F[lastIndx[0]][lastIndx[1]][lastIndx[2]] = bounds[1] ? beta[N]/(1 - alp[N]) : 0;
        //F[lastIndx[0]][lastIndx[1]][lastIndx[2]] = bounds[1] ? ((4-alp[N-1])*beta[N] - beta[N-1])/(3 - (4.0- alp[N-1])*alp[N]) : 0;
    }
    for(int i=N-1;i>=0;i--){
        int* indx = mesh.indxByDirection(i, s, k, direction);
        int* nextIndx = mesh.nextIndx(indx, direction);
        F[indx[0]][indx[1]][indx[2]] = alp[i+1]*argF[nextIndx] + beta[i+1];
    }
    delete []alp;
    delete []beta;
}

void approximation::cyclicThomasAlgorithm(double ***F,
                                          double *a,
                                          double *b,
                                          double *c,
                                          double *d,
                                          int i,
                                          int s)
{
    if (mesh.getJ()==0) {
        throw std::invalid_argument("Запрещено использование циклической прогонки в Декартовой системе координат");
        return;
    }
    int N = mesh.getN(3);
    double* alp = new double[N+1];
    double* beta = new double[N+1];
    double* gamma = new double[N+1];
    alp[1]=0;
    beta[1]=0;
    gamma[1]=1;
    for(int k=1;k<N;k++){
        alp[k+1] = b[k]/(c[k]-a[k]*alp[k]);
        beta[k+1]= (d[k] + a[k]*beta[k])/(c[k]-a[k]*alp[k]);
        gamma[k+1]= (a[k]*gamma[k])/(c[k]-a[k]*alp[k]);
    }
    double* V = new double[N+1];
    double* U = new double[N+1];
    U[0]=0;
    U[N]=0;
    V[0]=1;
    V[N]=1;
    for(int k=N-1;k>=0;k--){
        U[k]= alp[k+1]*U[k+1] + beta[k+1];
        V[k]= alp[k+1]*V[k+1] + gamma[k+1];
    }
    double coefficient = (d[0] + a[0]*beta[N] + b[0]*U[1])/(c[0] - a[0]*alp[N] - a[0]*gamma[N] - b[0]*V[1]);
    //coefficient = (d[0] + a[0]*U[N3-1] + b[0]*U[1])/(c[0] - a[0]*V[N3-1] - b[0]*V[1]);
    for(int k=0;k<=N;k++){
        F[i][s][k] = U[k] + coefficient*V[k];
    }
    delete []alp;
    delete []beta;
    delete []gamma;
    delete []U;
    delete []V;
}
